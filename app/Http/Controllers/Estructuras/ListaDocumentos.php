<?php
/**
 * Created by PhpStorm.
 * User: Harry J
 * Date: 8/6/2018
 * Time: 12:24 AM
 */
namespace App\Http\Controllers\Estructuras;

class ListaDocumentos{
    public $lista;
    public function __construct()
    {
        $this->lista = [];
    }
    public function parse($json_documentos,$id){
        $documentos = json_decode($json_documentos);
        for($i = 0; $i < count($documentos); $i++){
            $doc = new Documento($documentos[$i]->extencion,$id,$documentos[$i]->nombre);
            $doc->url = $documentos[$i]->url;
            $doc->tipo = $documentos[$i]->tipo;
            $this->lista[]= $doc;
        }
        return $this->lista;
    }
}