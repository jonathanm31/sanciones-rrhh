<?php
/**
 * Created by PhpStorm.
 * User: Harry J
 * Date: 8/6/2018
 * Time: 12:24 AM
 */
namespace App\Http\Controllers\Estructuras;

class Documento{
    public $nombre;
    public $url;
    public $extencion;
    public $tipo;

    public function __construct($ext,$id,$nombre){
        $ext = strtolower($ext);
        switch ($ext){
            case 'jpg':
                $tipo = '0';
                break;
            case 'png':
                $tipo = '0';
                break;
            case 'jpeg':
                $tipo = '0';
                break;
            default:
                $tipo = '1';
                break;
        }
        $this->url = "https://app.mapsalud.com/clientes/somosoh/sanciones/public/ocurrencias/".$id.'/'.$nombre;
        $this->nombre = $nombre;
        $this->extencion = $ext;
        $this->tipo = $tipo;
    }



}
