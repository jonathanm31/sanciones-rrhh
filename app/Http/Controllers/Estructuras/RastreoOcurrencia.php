<?php
/**
 * Created by PhpStorm.
 * User: Harry J
 * Date: 8/6/2018
 * Time: 12:46 AM
 */

namespace App\Http\Controllers\Estructuras;


class RastreoOcurrencia
{
    public  $idocurrencia;
    public  $detalle;
    public  $fecha_creacion;
    public  $tipo;
    public  $estado;
    public  $dni_colaborador;
    public  $dni_aprobador;
    private  $descripcion;

    public function __construct($tipo,$estado)
    {
        $this->set_tipo($tipo);
        $this->set_estado($estado);
        $this->estado = $estado;
    }

    private function set_tipo($tipo){
        switch ($tipo){
            case 1:
                $this->tipo = "creado";
                break;
            case 2:
                $this->tipo = "modificado";
                break;
            case 3:
                $this->tipo = "eliminado";
                break;
            case 4:
                $this->tipo = "ReLab";
                break;
        }
    }
    private function set_estado($estado){
        switch ($estado){
            case -1:
                $this->tipo = "BORRADOR";
                $this->descripcion = "No se ha enviado, se ha guardado, esta ocurrencia puede eliminarse";
                break;
            case 0:
                $this->tipo = "ENVIADA APROBADOR";
                if($this->tipo == "Relab"){
                    $this->descripcion = "Enviado a Relab";
                }else{
                    $this->descripcion = "Enviado a Aprobador";
                }
                break;
            case 1:
                $this->tipo = "RECHAZAR PARA MODIFICACION";
                $this->descripcion = "Documentacion pendiente...";
                break;
            case 2:
                $this->tipo = "MODIFICADA";
                $this->descripcion = "Documentacion subida...";
                break;
            case 3:
                $this->tipo = "ACEPTADA POR APROBADOR";
                $this->descripcion = "Se puede imprimir el formato de ocurrencia. Solo si la sanción no es suspensión, se debe enviar para su cierre.
                                        .Si es suspensión, se debe enviar a RELAB";
                break;
            case 4:
                $this->tipo = "DENEGADA POR APROBADOR";
                $this->descripcion = "Se debe cerrar la ocurrencia.";
                break;
            case 5:
                $this->tipo = "ACEPTADA por Relab";
                $this->descripcion = "Se puede imprimir el formato de ocurrencia.Se debe cerrar la ocurrencia.";
                break;
            case 6:
                $this->tipo = "DENEGADA por Relab";
                $this->descripcion = "Se debe cerrar la ocurrencia.";
                break;
            case 7:
                $this->tipo = "CERRADA";
                $this->descripcion = "";

                break;
        }
    }

}