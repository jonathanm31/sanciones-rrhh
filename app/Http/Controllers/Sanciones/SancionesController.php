<?php

namespace App\Http\Controllers\Sanciones;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Models\Sanciones;
use App\Http\Controllers\Controller;

class SancionesController extends Controller
{
    /*****************AUTH******************/

    public function index(Request $request){
        return view('Sanciones.sanciones');
    }
    public function get_all(Request $request){
        $sanciones = Sanciones::get();
        return json_encode(array('status'=> 200, 'data' => $sanciones));
    }
    function afiliado_info( $request){
        $query = $request['query'];
        $idadministrador = $request['idadministrador'];
        $usuario = Auth::user();
        if($usuario->idrol == 1){
            $afiliados = DB::table('afiliadoinfo_n')
                ->where('afiliadoinfo_n.nombres', 'like', '%'.$query.'%')
                ->orwhere('afiliadoinfo_n.idafiliado', '=', $query)
                ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
                ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')
                ->leftJoin('departamentos_n','secciones_n.iddepartamento','=','departamentos_n.iddepartamento')
                ->leftJoin('areas_n','departamentos_n.idarea','=','areas_n.idarea')
                ->join('adminafiliado','adminafiliado.idafiliado','=','afiliadoinfo_n.idafiliado')
                ->where('adminafiliado.idadministrador','=',$usuario->idadministrador)
                ->groupBy('afiliadoinfo_n.idafiliado')

                ->get();
        }else{
            $afiliados = DB::table('afiliadoinfo_n')
                ->where('afiliadoinfo_n.nombres', 'like', '%'.$query.'%')
                ->orwhere('afiliadoinfo_n.idafiliado', '=', $query)
                ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
                ->leftJoin('departamentos_n','secciones_n.iddepartamento','=','departamentos_n.iddepartamento')
                ->leftJoin('areas_n','departamentos_n.idarea','=','areas_n.idarea')
                ->leftJoin('areas_n','departamentos_n.iddepartamento','=','areas_n.iddepartamento')
                ->Join('adminafiliado','afiliadoinfo_n.idafiliado','=','adminafiliado.idafiliado')
                ->where('adminafiliado.idadministrador','=',$idadministrador)
                ->get();
        }

        if($afiliados) return json_encode(['status' => 200, 'data' => $afiliados]);
        return json_encode(['status'=> 100, 'mensaje' => 'no hay afiliados con ese nombre', 'data' => []]);

    }


    /***********CRUD***********/
    public function insertar(Request $request){
        $nombre = $request['nombre'];
        $sancion = new Sanciones;
        $sancion->sancion = $nombre;
        $sancion->state = $request['estado'];
        $sancion->save();
        return json_encode(array('state'=>200,'mensaje'=> 'Se Guardo Correctamente'));
    }
    public function modificar(Request $request){
        $nombre = $request['nombre'];
        $state = $request['estado'];
        $sancion = Sanciones::find($request['idsancion']);
        $sancion->sacion = $nombre;
        $sancion->state = $state;
        $sancion->save();
        return json_encode(array('state'=>200,'mensaje'=> 'Se modifico Correctamente'));

    }
    public function eliminar(Request $request){
        $sancion = Sanciones::find($request['idsancion']);
        $sancion->delete();
        return json_encode(array('state'=>200,'mensaje'=> 'Se elimino Correctamente'));
    }
}
