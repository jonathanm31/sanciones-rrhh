<?php

namespace App\Http\Controllers\Sanciones;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Models\Faltas;
use App\Models\Administrador;
use App\Models\Ocurrencias;
use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Http\Controllers\Sanciones;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use App\Http\Controllers\Estructuras;


class OcurrenciasController extends Controller
{
    private $server ='http://localhost:8000/';
    private  $servicios;


    public function __construct()
    {
        $servicios = new ServiciosWebController() ;
    }
    public function getServiciosInfo($servicio,$data){
        try{
            $url = $this->server.$servicio;
            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === FALSE) {
                return Redirect::to('login')->with('error','Error de servidor');
            }else{
                return $result;
            }}catch (Exception $e){
            return json_encode(['mensaje'=>'Upps, intentalo nuevamente!','status' => 100]);
        }
    }
    /**********VISTAS WEB********************/

    public function index(Request $request){

        $usuario = Auth::user();
        $servicio = new ServiciosWebController();
        $ocurrencias = $servicio->get_ocurrencias(["idadministrador" => $usuario->idadministrador]);
        $ocurrencias = json_decode($ocurrencias);

//        $ocurrencias = DB::table('ocurrencias_n')
//            ->select('ocurrencias_n.state as estado','ocurrencias_n.*','afiliadoinfo_n.*','puestos_n.puesto','secciones_n.seccion','sanciones_n.sancion','sanciones_n.idsancion','faltas_n.idfalta','faltas_n.falta')
//            ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
//            ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
//            ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
//            ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
//            ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')->simplePaginate(30);
      return view('Sanciones.ocurrencias')->with( 'data' , array('ocurrencias' => $ocurrencias->data));

    }




    public function rastreo(Request $request){
        $servicio = new ServiciosWebController();
        $ocurrencias = $servicio->rastreo($request);
        return $ocurrencias;
    }
    public function get_sancion(Request $request){
        $servicio = new ServiciosWebController();
        $sancion = $servicio->get_sancion($request);
        return $sancion;
    }
    public function get_faltas(Request $request){
        $servicio = new ServiciosWebController();
        $faltas = $servicio->search_faltas($request);
        return $faltas;
    }
    public function get_colaborador(Request $request){
        $servicio = new ServiciosWebController();
        $colaborador = $servicio->afiliado_info($request);
        return $colaborador;
    }

    public function qinsertar(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->insertar($request);
        return $crear;
    }
    public function qupload_file(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->upload_file($request);
        return $crear;
    }
    public function qenviar(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->enviar_gerencia($request);
        return $crear;
    }
    public function qeliminar(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->eliminar_ocurrencia($request);
        return $crear;
    }
    public function qpedirmodificacion(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->pedir_modificacion($request);
        return $crear;
    }
    public function qenviarmodifiacion(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->enviar_modificacion($request);
        return $crear;
    }
    public function qcerrar(Request $request){
            $servicio = new ServiciosWebController();
            $crear = $servicio->cerrado($request);
            return $crear;
    }
    public function qaceptado(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->aceptado_gerencia($request);
        return $crear;
    }
    public function qdenegado(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->denegado_gerencia($request);
        return $crear;
    }
    public function qaceptadorelab(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->aceptado_relab($request);
        return $crear;
    }
    public function qdenegadorelab(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->denegado_relab($request);
        return $crear;
    }
    public function qmodificar(Request $request){
        $servicio = new ServiciosWebController();
        $crear = $servicio->modificar($request);
        return $crear;
    }

    public function get_all(Request $request){
        $ocurrencias = DB::table('ocurrencias_n')
            ->select('ocurrencias_n.state as estado','ocurrencias_n.*','afiliadoinfo_n.*','puestos_n.puesto','secciones_n.seccion','sanciones_n.sancion','sanciones_n.idsancion','faltas_n.idfalta','faltas_n.falta')
            ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
            ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
            ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
            ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
            ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')->simplePaginate(15);

        return json_encode(array('status'=> 200, 'data' => array('ocurrencias' => $ocurrencias)));
    }
    public function nueva_ocurrencia(Request $request){

        $idadministrador =  Auth::User()->idadministrador;
        $faltas = DB::table('faltas_n')->get();
        $sanciones  = DB::table('sanciones_n')->get();

        if(Auth::User()->idrol == 1 ){
            $afiliados = DB::table('afiliadoinfo_n')->get();
        }else{
            $afiliados = DB::table('afiliadoinfo_n')
                ->Join('adminafiliado','afiliadoinfo_n.idafiliado','=','adminafiliado.idafiliado')
                ->where('adminafiliado.idadministrador','=',$idadministrador)->get();
        }

        return view('Sanciones.nueva_ocurrencias')->with( 'data' , array('faltas' => $faltas, 'sanciones' => $sanciones, 'afiliados' => $afiliados));
    }

    public function editar_ocurrencia(Request $request){
        $idocurrencia = $request['ocurrencia'];
        $ocurrencia = DB::table('ocurrencias_n')
            ->select('ocurrencias_n.state as estado','ocurrencias_n.*','afiliadoinfo_n.*','puestos_n.puesto','secciones_n.seccion','sanciones_n.sancion','sanciones_n.idsancion','faltas_n.idfalta','faltas_n.falta')
            ->where('ocurrencias_n.idocurrencia','=',$idocurrencia)
            ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
            ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
            ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
            ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
            ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')->first();

        $logs = DB::table('logsanciones_n')
            ->select('ocurrencias_n.idocurrencia','logsanciones_n.descripcion','logsanciones_n.created_at','logsanciones_n.tipo',
                'logsanciones_n.estado_ocurrencia','logsanciones_n.idcolaborador','logsanciones_n.idaprobador')
            ->join('ocurrencias_n','logsanciones_n.idocurrencia','=','ocurrencias_n.idocurrencia')
            ->where('ocurrencias_n.idocurrencia','=',$idocurrencia)->get();

        $tracer = [];
        foreach ($logs as $log){
            $rastreo = new Estructuras\RastreoOcurrencia($log->tipo,$log->estado_ocurrencia);
            $rastreo->idocurrencia = $log->idocurrencia;
            $rastreo->detalle = $log->descripcion;
            $rastreo->dni_aprobador = $log->idaprobador;
            $rastreo->dni_colaborador = $log->idcolaborador;
            $rastreo->fecha_creacion = $log->created_at;
            $tracer []= $rastreo;
        }

        return view('Sanciones.modificar_ocurrencias')->with( array('ocurrencia'=>$ocurrencia,'logs' => $tracer));

    }

    /****************FUNCIONALIDADES API**********************/
    public function get_all_filtro(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);
        $idadmin = $request['idadministrador'];

        $usuario = DB::table('administrador')->where('idadministrador','=',$idadmin)->first();

        $desde = $request['desde'];
        $hasta = $request['hasta'];
        $idadministrador = $request['idadministrador'];
        $idocurrencia = $request['idocurrencia'];
        $query = $request['query'];
        $tam_pag = $request['tam_pag'];
        if(!$tam_pag) $tam_pag = 15;
        //$page = $request["page"];

        if($desde == "-1")$desde = "";
        if($hasta == "-1")$hasta = "";
        if($idocurrencia == "-1")$idocurrencia = "";
        if($query == "-1")$query = "";

        if($usuario->idrol == 1){
            $ocurrencias = DB::table('ocurrencias_n')
                ->select('ocurrencias_n.state as estado','ocurrencias_n.*','afiliadoinfo_n.*','puestos_n.puesto','secciones_n.seccion','sanciones_n.sancion','sanciones_n.idsancion','faltas_n.idfalta','faltas_n.falta')
                ->orwhere('afiliadoinfo_n.nombres','like','%'.$query.'%')
                ->orwhere('afiliadoinfo_n.idafiliado','=',$query)
                ->orwhere('ocurrencias_n.idocurrencia','=',$idocurrencia)
                ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
                ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
                ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
                ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
                ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')->simplePaginate($tam_pag);
        }else{
            $ocurrencias = DB::table('ocurrencias_n')
                ->select('ocurrencias_n.state as estado','ocurrencias_n.*','afiliadoinfo_n.*','puestos_n.puesto','secciones_n.seccion','sanciones_n.sancion','sanciones_n.idsancion','faltas_n.idfalta','faltas_n.falta')
                ->orwhere('afiliadoinfo_n.nombres','like','%'.$query.'%')
                ->orwhere('afiliadoinfo_n.idafiliado','=',$query)
                ->orwhere('ocurrencias_n.idocurrencia','=',$idocurrencia)
                ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
                ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
                ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
                ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
                ->Join('adminafiliado','afiliadoinfo_n.idafiliado','=','adminafiliado.idafiliado')
                ->where('adminafiliado.idadministrador','=',$idadministrador)
                ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')->simplePaginate($tam_pag);
        }

        foreach ($ocurrencias as $index => $ocurrencia){
            $documentos = json_decode($ocurrencia->documentos);
            if($documentos){
                foreach ($documentos as $index => $documento){
                    $pos = strpos($documento,".",1);
                    $ext = strtolower(substr($documento, $pos+1, 5));
                    $tipo = '';
                    switch ($ext){
                        case 'jpg':
                            $tipo = '0';
                            break;
                        case 'png':
                            $tipo = '0';
                            break;
                        case 'jpeg':
                            $tipo = '0';
                            break;
                        default:
                            $tipo = '1';
                            break;
                    }

                    $documentos[$index] = [ 'url' => "https://app.mapsalud.com/clientes/somosoh/admin/public/ocurrencias/".$ocurrencia->idocurrencia.$documento,
                        'nombre' => $documento,
                        'extencion' => $ext,
                        'tipo' => $tipo
                    ];
                }
                $ocurrencia->documentos = $documentos;
            }else{
                $ocurrencia->documentos = [];
            }

            $calificacion = json_decode($ocurrencia->calificacion);
            if($calificacion == null or $calificacion == -1)$calificacion = array();
            foreach ($calificacion as $index => $c){
                $calificacion[$index] = DB::table('calificaciones_n')->where('idcalificacion','=', $c)->first();
            }
            $ocurrencia->calificacion = $calificacion;
        }
        return json_encode(array('status'=> 200, 'data' => array('ocurrencias' => $ocurrencias)));
    }

    public function modificar(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);
        $iddispositivo = $request['iddispositivo'];

        $ocurrencia = Ocurrencias::find($request['idocurrencia']);
        if(!$request['idocurrencia']){
            return json_encode(array('state'=>100,'mensaje'=> 'Campos incompletos'));
        }
        if( $request['afiliado']){
            return json_encode(array('state'=>100,'mensaje'=> 'Si desea cambiar de afiliado, debe eliminar la ocurrencia actual y crear una nueva con el afiliado correspondiente'));
        }
        if($request['falta'])$ocurrencia->idfalta = $request['falta'];
        if($request['sancion'])$ocurrencia->idsancion = $request['sancion'];
        if($request['descripcion'])$ocurrencia->descripcion = $request['descripcion'];
        if($request['calificacion']) $ocurrencia->calificacion = $request['calificacion'];
        if($request['fecha'])$ocurrencia->fec_ocurrencia = $request['fecha'];
        if($request['inicio'])$ocurrencia->fec_suspencion_ini = $request['inicio'];
        if($request['fin'])$ocurrencia->fec_suspencion_fin = $request['fin'];
        $ocurrencia->save();
        $this->crea_log($ocurrencia->idocurrencia,$request['idadministrador'],1,$iddispositivo,$ocurrencia->state);

        return json_encode(array('state'=>200,'mensaje'=> 'Se modifico Correctamente'));

    }
    public function eliminar(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);
        $iddispositivo = $request['iddispositivo'];

        $ocurrencia = Ocurrencias::find($request['idocurrencia']);
        $path = 'ocurrencias/'.$request['idocurrencia'];
        if(is_dir($path)) {
            array_map('unlink', glob($path."/*.*"));
            rmdir($path);
        }
        $this->crea_log($ocurrencia->idocurrencia,$request['idadministrador'],2,$iddispositivo,$ocurrencia->state);

        $ocurrencia->delete();
        return json_encode(array('state'=>200,'mensaje'=> 'Se elimino Correctamente'));

    }

    /*************FUNCIONALIDAD DE LA WEB*******************/




    /***********CREACION DE LOS LOG************************/





}
