<?php

namespace App\Http\Controllers\Sanciones;

use App\Models\Ocurrencias;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Administrador;
use App\Http\Controllers\Estructuras;
use Illuminate\Support\Facades\Auth;
use Dompdf\Dompdf;
use DateTime;

class ReportesController extends Controller
{
    function __construct()
    {
    }
    public function home(){
        $repo1 = $this->get_prom_faltas();
        $repo2 = $this->get_prom_sanciones();
        $repo3 = $this->get_frec_areas();
        $repo4 = $this->get_frec_ocurrencias();
        return view('home')->with('reportes' ,[$repo1,$repo2,$repo3,$repo4 ]);
    }
    public function get_prom_faltas(){
        $prom = DB::table('avg_faltas_central')->get();
        $prom1 = DB::table('avg_faltas_finantienda')->get();
        return  [ 'central' => $prom, 'finantienda' => $prom1 ];
    }
    public function get_prom_sanciones(){
        $prom = DB::table('avg_sanciones_central')->orderBy('sancion', 'asc')->get();
        $prom1 = DB::table('avg_sanciones_finantienda')->orderBy('sancion', 'asc')->get();
        return [ 'central' => $prom, 'finantienda' => $prom1 ];
    }
    public function get_frec_areas(){
        $prom = DB::table('frec_areas_central')->orderBy('frecuencia', 'desc')->get();
        $prom1 = DB::table('frec_areas_finantienda')->orderBy('frecuencia', 'desc')->get();
        return [ 'central' => $prom, 'finantienda' => $prom1 ];
    }
    public function get_frec_ocurrencias(){
        $prom = DB::table('frec_ocurrencias_central')->get();
        $prom1 = DB::table('frec_ocurrencias_finantienda')->get();
        return [ 'central' => $prom, 'finantienda' => $prom1 ];
    }

}