<?php

namespace App\Http\Controllers\Sanciones;

use App\Models\Ocurrencias;
use App\Models\Sanciones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Administrador;
use App\Http\Controllers\Estructuras;
use Illuminate\Support\Facades\Auth;
use App\Models\Faltas;
use App\Models\Log;
use DateTime;


class ServiciosWebController
{
    function __construct()
    {
    }
     public function send_mail_html($to,$subject,$message){
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <soporte@mapsalud.com>' . "\r\n";
        $headers .= "Reply-To: soporte@mapsalud.com\r\n";
        $headers .= "Return-Path: soporte@mapsalud.com\r\n";
        $headers .= "X-Priority: 3\r\n";
        $headers .= "X-Mailer: PHP". phpversion() ."\r\n";
       $headers .= "-odb -f soporte@mapsalud.com";

        $r=mail($to,$subject,$message,$headers);
        return $r;
    }
    /*******AUTENTICACION Y VALIDACION********************/
    function generateRandomString($length = 50) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_=-+';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function search_faltas( $request){
        $query = $request['query'];
        $faltas = Faltas::all();
        return json_encode(['status' => 200, 'data' => $faltas]);
    }
    function afiliado_info( $request){
        $query = $request['query'];
        $usuario = Auth::user();
        if($usuario->idrol == 4 || $usuario->idrol == 3 ){
            $afiliados = DB::table('afiliadoinfo_n')
                ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
                ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')
                ->leftJoin('areas_n','afiliadoinfo_n.idarea','=','areas_n.idarea')
                ->leftjoin('colaborador_registrador','afiliadoinfo_n.idafiliado','=','colaborador_registrador.idafiliado')
                ->where('afiliadoinfo_n.nombres', 'like', '%'.$query.'%')
                ->orwhere('afiliadoinfo_n.idafiliado', '=', $query)
                ->groupBy('afiliadoinfo_n.idafiliado')
                ->get();
        }else{
            $afiliados = DB::table('afiliadoinfo_n')
                ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
                ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')
                ->leftJoin('areas_n','afiliadoinfo_n.idarea','=','areas_n.idarea')
                ->join('colaborador_registrador','afiliadoinfo_n.idafiliado','=','colaborador_registrador.idafiliado')
                ->where('colaborador_registrador.idadministrador','=',$usuario->dni)
                ->where('afiliadoinfo_n.nombres', 'like', '%'.$query.'%')
                ->orwhere('afiliadoinfo_n.idafiliado', '=', $query)
                ->groupBy('afiliadoinfo_n.idafiliado')
                ->get();
        }

        if($afiliados) return json_encode(['status' => 200, 'data' => $afiliados]);
        return json_encode(['status'=> 100, 'mensaje' => 'no hay afiliados con ese nombre', 'data' => []]);

    }

    public function correos_solicitados($idocurrencia,$mensaje){
        $correos = [ "compensacionesresuelve@somosoh.pe", "javier.marzal@somosoh.pe"];
        foreach($correos as $correo){
            $json_msj = $this->send($idocurrencia,$correo,$mensaje);
        }
    }
    /*******FLUJO DE TRABAJO******************/
    public function insertar($request){
        $ocurrencia = new Ocurrencias;
        $admin = Auth::user()->dni;

        if(!$request['falta'] || !$request['sancion']  || !$request['fecha']){
            return json_encode(array('state'=>100,'mensaje'=> 'Campos incompletos'));
        }
        if($request['sancion'] == "1"){
            $inicio = $request['inicio'];
            $fin = $request['fin'];
            if($inicio == "-1")$inicio = "";
            if($fin == "-1")$fin = "";
            $ocurrencia->fec_suspencion_ini = $inicio;
            $ocurrencia->fec_suspencion_fin = $fin;
        }
        $ocurrencia->idafiliado = $request['idcolaborador'];
        $ocurrencia->idregistrador = $admin;
        $ocurrencia->idfalta = $request['falta'];
        $ocurrencia->idsancion = $request['sancion'];
        $ocurrencia->descripcion = $request['descripcion'];
        $ocurrencia->state = -1;
        $ocurrencia->reincidencia = 1;
        $ocurrencia->fec_ocurrencia = $request['fecha'];
        $ocurrencia->save();

        $this->crear_ocurrencia($request, $ocurrencia);
        return json_encode(array('status'=>200,'mensaje'=> 'Se Guardo Correctamente','data' => ['idocurrencia' => $ocurrencia->idocurrencia]));
    }
    public function crear_ocurrencia( $request, $ocurrencia){
        $idfuncionalidad = 6;
        $usuario = Auth::user();
        if( ($usuario->permisos >> $idfuncionalidad) % 2 == 0)return json_encode(['status' => 100, 'mensaje' => 'No tiene permiso']);
        if(!$request['falta'] || !$request['sancion']  || !$request['fecha']){
            return json_encode(array('state'=>100,'mensaje'=> 'Campos incompletos'));
        }
        $idcolaborador = $request['idcolaborador'];
        if($usuario->idrol == 4){
            $tipo = 5; // 5 creado por superadmin
        }else{
            $tipo = 0;
        }

        $log = new Log ;

        $log->idocurrencia = $ocurrencia->idocurrencia;
        $log->tipo = $tipo;
        $log->idfalta = $ocurrencia->idfalta;
        $log->estado_ocurrencia = $ocurrencia->state;
        $log->idcolaborador = $idcolaborador;
        $log->idaprobador = $usuario->dni;
        $log->iddispositivo = $request['iddispositivo'];
        $log->save();

        return json_encode(['status' => 200, 'mensaje' => "Ocurrencia Creada!"]);

    }
    public function enviar_gerencia( $request){
        $idfuncionalidad = 3;
        $usuario = Auth::user();
        if( ($usuario->permisos >> $idfuncionalidad) % 2 == 0)return json_encode(['status' => 100, 'mensaje' => 'No tiene permiso']);
        $ocurrencia = Ocurrencias::where('idocurrencia','=',$request['idocurrencia'])->first();
        $idcolaborador = $ocurrencia->idafiliado;
        if($usuario->idrol == 4){
            $aprobador = $usuario;
            $tipo = $request['tipo'];
            if($tipo == 4){
                $ocurrencia->state = 8;
            }else{
                $ocurrencia->state = 0;
            }

            $ocurrencia->save();
            if($tipo !== 4) $tipo = 5; // enviado por admin
            $log = new Log ;
            $log->idocurrencia = $ocurrencia->idocurrencia;
            $log->tipo = $tipo;
            $log->idfalta = $ocurrencia->idfalta;
            $log->estado_ocurrencia = $ocurrencia->state;
            $log->idcolaborador = $idcolaborador;
            $log->idaprobador = $usuario->dni;
            $log->iddispositivo = $request['iddispositivo'];
            $log->save();
        }else{
            $idaprobador = DB::table('registradorgerente_n')->where('idregistrador','=',$usuario->dni)->first();
            $aprobador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idaprobador->idgerente)->first();
            $ocurrencia->state = 0;
            $ocurrencia->save();
            $tipo = $request['tipo'];
            if($tipo !== "4") $tipo = 1;
            $log = new Log ;
            $log->idocurrencia = $ocurrencia->idocurrencia;
            $log->tipo = $tipo;
            $log->idfalta = $ocurrencia->idfalta;
            $log->estado_ocurrencia = $ocurrencia->state;
            $log->idcolaborador = $idcolaborador;
            $log->idaprobador = $usuario->dni;
            $log->iddispositivo = $request['iddispositivo'];
            $log->save();
        }
        $colaborador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idcolaborador)->first();
        $json_msj = $this->send($ocurrencia->idocurrencia,$usuario->email,"Ocurrencia Enviado a Aprobador");
        $json_msj = $this->send($ocurrencia->idocurrencia,$aprobador->email,"Ocurrencia Enviado a Aprobador");
        return json_encode(['status' => 200, 'mensaje' => "Ocurrencia enviada a Aprobador"]);
    }
    public function pedir_modificacion( $request){
        $idfuncionalidad = 5;
        $usuario = Auth::user();
        if( ($usuario->permisos >> $idfuncionalidad) % 2 == 0)return json_encode(['status' => 100, 'mensaje' => 'No tiene permiso']);
        $ocurrencia = Ocurrencias::where('idocurrencia','=',$request['idocurrencia'])->first();

        $log_ant = Log::where('idocurrencia','=',$ocurrencia->idocurrencia)->where('estado_ocurrencia','=',-1)->first();
        $registrador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$log_ant->idaprobador)->first();
        $idcolaborador = $ocurrencia->idafiliado;
        $ocurrencia->state = 1;
        $tipo = $request['tipo'];
        if($tipo !== 4) $tipo = 1;
        $ocurrencia->save();
        $log = new Log ;
        $log->idocurrencia = $ocurrencia->idocurrencia;
        $log->tipo = $tipo;
        $log->idfalta = $ocurrencia->idfalta;
        $log->estado_ocurrencia = $ocurrencia->state;
        $log->descripcion = $request['descripcion'];
        $log->idcolaborador = $idcolaborador;
        $log->idaprobador = $usuario->dni;
        $log->iddispositivo = $request['iddispositivo'];
        $log->save();

        $colaborador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idcolaborador)->first();

        $json_msj = $this->send($ocurrencia->idocurrencia,$usuario->email,"Ocurrencia Se ha pedido la Modificacion");
        $json_msj = $this->send($ocurrencia->idocurrencia,$registrador->email,"Ocurrencia Se ha pedido la Modificacion");
        return json_encode(['status' => 200, 'mensaje' => 'Se ha pedido la Modificacion satisfactoriamente']);
    }
    public function enviar_modificacion( $request){
        $idfuncionalidad = 3;
        $usuario = Auth::user();
        if( ($usuario->permisos >> $idfuncionalidad) % 2 == 0)return json_encode(['status' => 100, 'mensaje' => 'No tiene permiso']);

        $ocurrencia = Ocurrencias::where('idocurrencia','=',$request['idocurrencia'])->first();
        $idcolaborador = $ocurrencia->idafiliado;
        $idaprobador = DB::table('registradorgerente_n')->where('idregistrador','=',$usuario->dni)->first();
        $aprobador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idaprobador->idgerente)->first();

        $ocurrencia->state = 2;
        $ocurrencia->save();
        $log = new Log ;
        $log->idocurrencia = $ocurrencia->idocurrencia;
        $log->tipo = 1;
        $log->idfalta = $ocurrencia->idfalta;
        $log->estado_ocurrencia = $ocurrencia->state;
        $log->descripcion = $request['descripcion'];
        $log->idcolaborador = $idcolaborador;
        $log->idaprobador = $usuario->dni;
        $log->iddispositivo = $request['iddispositivo'];
        $log->save();
        $colaborador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idcolaborador)->first();
        $json_msj = $this->send($ocurrencia->idocurrencia,$usuario->email,"Ocurrencia Se ha Modificacion");
        $json_msj = $this->send($ocurrencia->idocurrencia,$aprobador->email,"Ocurrencia Se ha Modificacion");
        return json_encode(['status' => 200, 'mensaje' => 'Se ha enviado satisfactoriamente']);
    }
    public function aceptado_gerencia( $request){
        $idfuncionalidad = 4;
        $usuario = Auth::user();
        if( ($usuario->permisos >> $idfuncionalidad) % 2 == 0)return json_encode(['status' => 100, 'mensaje' => 'No tiene permiso']);

        $ocurrencia = Ocurrencias::where('idocurrencia','=',$request['idocurrencia'])->first();
        $idcolaborador = $ocurrencia->idafiliado;
        $colaborador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idcolaborador)->first();

        $log_ant = Log::where('idocurrencia','=',$ocurrencia->idocurrencia)->where('estado_ocurrencia','=',-1)->first();
        $registrador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$log_ant->idaprobador)->first(); // quien refistro
        $ocurrencia->state = 3;
        $ocurrencia->save();
        $log = new Log ;
        $log->idocurrencia = $ocurrencia->idocurrencia;

        if($usuario->idrol == 4){
            $log->tipo = 5;
        }else {
            $log->tipo = 1;
        }
            $log->idfalta = $ocurrencia->idfalta;
            $log->estado_ocurrencia = $ocurrencia->state;
            $log->descripcion = $request['descripcion'];
            $log->idcolaborador = $idcolaborador;
            $log->idaprobador = $usuario->dni;
            $log->iddispositivo = $request['iddispositivo'];
            $log->save();
        if($ocurrencia->sancion == 1){
            $request['tipo'] = 4;
            $this->enviar_gerencia($request);
        }
        $mensaje = "Ocurrencia se acepto por aprobador";
        /****CORREOS SOLICITADOS****/
        $this->correos_solicitados($ocurrencia->idocurrencia,$mensaje);
        /*****fin correos******/
        $json_msj = $this->send($ocurrencia->idocurrencia,$usuario->email,"Ocurrencia se acepto por aprobador");
        $json_msj = $this->send($ocurrencia->idocurrencia,$registrador->email,"Ocurrencia se acepto por aprobador");
        return json_encode(['status' => 200, 'mensaje' => 'Se acepto satisfactoriamente']);
    }
    public function denegado_gerencia( $request){
        $idfuncionalidad = 4;
        $usuario = Auth::user();
        if( ($usuario->permisos >> $idfuncionalidad) % 2 == 0)return json_encode(['status' => 100, 'mensaje' => 'No tiene permiso']);

        $ocurrencia = Ocurrencias::where('idocurrencia','=',$request['idocurrencia'])->first();
        $idcolaborador = $ocurrencia->idafiliado;
        $colaborador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idcolaborador)->first();

        $log_ant = Log::where('idocurrencia','=',$ocurrencia->idocurrencia)->where('estado_ocurrencia','=',-1)->first();
        $registrador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$log_ant->idaprobador)->first();

        $ocurrencia->state = 4;
        $ocurrencia->save();
        $log = new Log ;
        $log->idocurrencia = $ocurrencia->idocurrencia;
        if($usuario->idrol == 4){
            $log->tipo = 5;
        }else {
            $log->tipo = 1;
        }
        $log->idfalta = $ocurrencia->idfalta;
        $log->estado_ocurrencia = $ocurrencia->state;
        $log->descripcion = $request['descripcion'];
        $log->idcolaborador = $idcolaborador;
        $log->idaprobador = $usuario->dni;
        $log->iddispositivo = $request['iddispositivo'];
        $log->save();

        $mensaje = "Ocurrencia se denego por aprobador";
        /****CORREOS SOLICITADOS****/
        $this->correos_solicitados($ocurrencia->idocurrencia,$mensaje);
        /*****fin correos******/
        $json_msj = $this->send($ocurrencia->idocurrencia,$usuario->email,"Ocurrencia se denego por aprobador");
        $json_msj = $this->send($ocurrencia->idocurrencia,$registrador->email,"Ocurrencia se acepto por aprobador");
        return json_encode(['status' => 200, 'mensaje' => 'Se denego satisfactoriamente']);
    }
    public function aceptado_relab( $request){
        $idfuncionalidad = 4;
        $usuario = Auth::user();
        if( ($usuario->permisos >> $idfuncionalidad) % 2 == 0)return json_encode(['status' => 100, 'mensaje' => 'No tiene permiso']);

        $ocurrencia = Ocurrencias::where('idocurrencia','=',$request['idocurrencia'])->first();
        $idcolaborador = $ocurrencia->idafiliado;
        $colaborador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idcolaborador)->first();
        $log_ant = Log::where('idocurrencia','=',$ocurrencia->idocurrencia)->where('estado_ocurrencia','=',-1)->first();
        $registrador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$log_ant->idaprobador)->first();

        /*****/
        $log_ant = Log::where('idocurrencia','=',$ocurrencia->idocurrencia)->where('estado_ocurrencia','=',3)->first();
        $aprobador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$log_ant->idaprobador)->first();

        $ocurrencia->state = 5;
        $ocurrencia->save();
        $log = new Log ;
        $log->idocurrencia = $ocurrencia->idocurrencia;
        $log->tipo = 1;
        $log->idfalta = $ocurrencia->idfalta;
        $log->tipo = 4;
        $log->estado_ocurrencia = $ocurrencia->state;
        $log->descripcion = $request['descripcion'];
        $log->idcolaborador = $idcolaborador;
        $log->idaprobador = $usuario->dni;
        $log->iddispositivo = $request['iddispositivo'];
        $log->save();
        $mensaje = "Ocurrencia se acepto por relab";
        /****CORREOS SOLICITADOS****/
        $this->correos_solicitados($ocurrencia->idocurrencia,$mensaje);
        /*****fin correos******/


        $json_msj = $this->send($ocurrencia->idocurrencia,$usuario->email,"Ocurrencia se aprobado por relab");
        $json_msj = $this->send($ocurrencia->idocurrencia,$registrador->email,"Ocurrencia se aprobado por relab");
        $json_msj = $this->send($ocurrencia->idocurrencia,$aprobador->email,"Ocurrencia se aprobado por relab");
        return json_encode(['status' => 200, 'mensaje' => 'Aceptado por Relab satisfactoriamente']);
    }
    public function denegado_relab( $request){
        $idfuncionalidad = 4;
        $usuario = Auth::user();
        if( ($usuario->permisos >> $idfuncionalidad) % 2 == 0)return json_encode(['status' => 100, 'mensaje' => 'No tiene permiso']);

        $ocurrencia = Ocurrencias::where('idocurrencia','=',$request['idocurrencia'])->first();
        $idcolaborador = $ocurrencia->idafiliado;
        $colaborador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idcolaborador)->first();
        $log_ant = Log::where('idocurrencia','=',$ocurrencia->idocurrencia)->where('estado_ocurrencia','=',-1)->first();
        $registrador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$log_ant->idaprobador)->first();
        /*****/
        $log_ant = Log::where('idocurrencia','=',$ocurrencia->idocurrencia)->where('estado_ocurrencia','=',3)->first();
        $aprobador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$log_ant->idaprobador)->first();

        $ocurrencia->state = 6;
        $ocurrencia->save();
        $log = new Log ;
        $log->idocurrencia = $ocurrencia->idocurrencia;
        $log->tipo = 1;
        $log->idfalta = $ocurrencia->idfalta;
        $log->tipo = 4;
        $log->estado_ocurrencia = $ocurrencia->state;
        $log->descripcion = $request['descripcion'];
        $log->idcolaborador = $idcolaborador;
        $log->idaprobador = $usuario->dni;
        $log->iddispositivo = $request['iddispositivo'];
        $log->save();

        $mensaje = "Ocurrencia se denego por relab";
        /****CORREOS SOLICITADOS****/
        $this->correos_solicitados($ocurrencia->idocurrencia,$mensaje);
        /*****fin correos******/
        $json_msj = $this->send($ocurrencia->idocurrencia,$usuario->email,"Ocurrencia se denego por relab");
        $json_msj = $this->send($ocurrencia->idocurrencia,$registrador->email,"Ocurrencia se denego por relab");
        $json_msj = $this->send($ocurrencia->idocurrencia,$aprobador->email,"Ocurrencia se denego por relab");
        return json_encode(['status' => 200, 'mensaje' => 'Se Denego por Relab satisfactoriamente']);
    }
    public function cerrado( $request){
        $idfuncionalidad = 4;
        $usuario = Auth::user();
//        if( ($usuario->permisos >> $idfuncionalidad) % 2 == 0)return json_encode(['status' => 100, 'mensaje' => 'No tiene permiso']);

        $ocurrencia = Ocurrencias::where('idocurrencia','=',$request['idocurrencia'])->first();
        $idcolaborador = $ocurrencia->idafiliado;
        $colaborador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idcolaborador)->first();

        if($usuario->idrol == 4){
            $aprobador = $usuario;

        }else{
            $idaprobador = DB::table('registradorgerente_n')->where('idregistrador','=',$usuario->dni)->first();
            $aprobador = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idaprobador->idgerente)->first();
        }


        $ocurrencia->state = 7;
        $ocurrencia->save();
        $log = new Log ;
        $log->idocurrencia = $ocurrencia->idocurrencia;
        $log->tipo = 1;
        $log->idfalta = $ocurrencia->idfalta;
        $log->tipo = 4;
        $log->estado_ocurrencia = $ocurrencia->state;
        $log->descripcion = $request['descripcion'];
        $log->idcolaborador = $idcolaborador;
        $log->idaprobador = $usuario->dni;
        $log->iddispositivo = $request['iddispositivo'];
        $log->save();

        $mensaje = "Ocurrencia se ha cerrado";
        /****CORREOS SOLICITADOS****/
        $this->correos_solicitados($ocurrencia->idocurrencia,$mensaje);
        /*****fin correos******/


        $json_msj = $this->send($ocurrencia->idocurrencia,$usuario->email,"Ocurrencia se cerro");
        $json_msj = $this->send_colab($ocurrencia->idocurrencia,$colaborador->email,"Ocurrencia se cerro");
        $json_msj = $this->send($ocurrencia->idocurrencia,$aprobador->email,"Ocurrencia se cerro");

        return json_encode(['status' => 200, 'mensaje' => 'Se Cerro la ocurrencia']);
    }
    public function send_colab($idocurrencia, $to, $estado){
        //$idocurrencia, $to,$estado
        $ocurrencia = DB::table('ocurrencias_n')
            ->select('ocurrencias_n.state as estado','areas_n.area','faltas_n.gravedad','ocurrencias_n.*','afiliadoinfo_n.*','puestos_n.puesto','secciones_n.seccion','sanciones_n.sancion','sanciones_n.idsancion','faltas_n.idfalta','faltas_n.falta')
            ->where('ocurrencias_n.idocurrencia','=',$idocurrencia)
            ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
            ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
            ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
            ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
            ->leftJoin('areas_n','afiliadoinfo_n.idarea','=','areas_n.idarea')
            ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')->first();

        $idfalta = $ocurrencia->idfalta;
        $falta = Faltas::find($idfalta);
        $legal = json_decode($falta->aplica_ids);
        $leyes = [];
        foreach ($legal as $ley){
            $l = DB::table('rif_n')
                ->select("rif_n.tipo as hijo" , "rif_n.titulo", "rif_n.descripcion", "rif.tipo as padre",
                    "rif.titulo as pnombre", "rif.descripcion as pdescripcion",
                    "r.tipo as abuelo", "r.titulo as anombre", "r.descripcion as adescripcion")
                ->leftJoin("rif_n as rif","rif_n.idparent" ,"=" ,"rif.idrif")
                ->leftJoin("rif_n as r","rif.idparent","=","r.idrif")
                ->where("rif_n.idrif", "=" ,$ley)
                ->get();
            $leyes []= $l;
        }
        $ocurrencia->legal = $leyes;


        setlocale(LC_ALL,"es_ES");
        $string = date_create($ocurrencia->fec_ocurrencia);
        $string = date_format($string, 'd/m/Y');
        $date =  DateTime::createFromFormat("d/m/Y", $string);
        $fecha =   strftime(("%A %d de %B del %Y"),$date->getTimestamp());

        $vista = view('Sanciones/emails/send_colab')->with(['fecha'=>$fecha,'ocurrencia' => $ocurrencia,'estado'=>$estado]);
        $vista_html = $vista->render();

        $r = $this->send_mail_html($to,'Ocurrencia #'.$ocurrencia->idocurrencia,$vista_html);
        return $r;

    }
    public function send($idocurrencia, $to,$estado){
        //$idocurrencia, $to,$estado
        $ocurrencia = DB::table('ocurrencias_n')
            ->select('ocurrencias_n.state as estado','areas_n.area','faltas_n.gravedad','ocurrencias_n.*','afiliadoinfo_n.*','puestos_n.puesto','secciones_n.seccion','sanciones_n.sancion','sanciones_n.idsancion','faltas_n.idfalta','faltas_n.falta')
            ->where('ocurrencias_n.idocurrencia','=',$idocurrencia)
            ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
            ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
            ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
            ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
            ->leftJoin('areas_n','afiliadoinfo_n.idarea','=','areas_n.idarea')
            ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')->first();

        $idfalta = $ocurrencia->idfalta;
        $falta = Faltas::find($idfalta);
        $legal = json_decode($falta->aplica_ids);
        $leyes = [];
        foreach ($legal as $ley){
            $l = DB::table('rif_n')
                ->select("rif_n.tipo as hijo" , "rif_n.titulo", "rif_n.descripcion", "rif.tipo as padre",
                    "rif.titulo as pnombre", "rif.descripcion as pdescripcion",
                    "r.tipo as abuelo", "r.titulo as anombre", "r.descripcion as adescripcion")
                ->leftJoin("rif_n as rif","rif_n.idparent" ,"=" ,"rif.idrif")
                ->leftJoin("rif_n as r","rif.idparent","=","r.idrif")
                ->where("rif_n.idrif", "=" ,$ley)
                ->get();
            $leyes []= $l;
        }
        $ocurrencia->legal = $leyes;


        setlocale(LC_ALL,"es_ES");
        $string = date_create($ocurrencia->fec_ocurrencia);
        $string = date_format($string, 'd/m/Y');
        $date =  DateTime::createFromFormat("d/m/Y", $string);
        $fecha =   strftime(("%A %d de %B del %Y"),$date->getTimestamp());

         $vista = view('Sanciones/emails/send')->with(['fecha'=>$fecha,'ocurrencia' => $ocurrencia,'estado'=>$estado]);
         $vista_html = $vista->render();

         $r = $this->send_mail_html($to,'Ocurrencia #'.$ocurrencia->idocurrencia,$vista_html);
         return $r;

    }
   
    /********CRUD OCURRENCIAS************/
    public function get_ocurrencia( $request){
        $idocurrencia = $request['idocurrencia'];
        $ocurrencia = Ocurrencias::select('*','state as estado')->find($idocurrencia);
        $ocurrencia->documentos = json_decode($ocurrencia->documentos );

        $idfalta = $ocurrencia->idfalta;
        $falta = Faltas::find($idfalta);
        $legal = json_decode($falta->aplica_ids);
        $leyes = [];
        foreach ($legal as $ley){
            $l = DB::table('rif_n')
                ->select("rif_n.tipo as hijo" , "rif_n.titulo", "rif_n.descripcion", "rif.tipo as padre", "rif.titulo as pnombre", "rif.descripcion as pdescripcion",
                    "r.tipo as abuelo", "r.titulo as anombre", "r.descripcion as adescripcion")
                ->leftJoin("rif_n as rif","rif_n.idparent" ,"=" ,"rif.idrif")
                ->leftJoin("rif_n as r","rif.idparent","=","r.idrif")
                ->where("rif_n.idrif", "=" ,$ley)
                ->get();
            $leyes []= $l;
        }
        $ocurrencia->legal = $leyes;
        return json_encode(["status" => 200,"data" => $ocurrencia]);

    }
    public function eliminar_ocurrencia( $request){
        $idocurrencia = $request['idocurrencia'];
        $ocurrencia = Ocurrencias::find($idocurrencia);
        if($ocurrencia->state == -1){
            $ocurrencia->delete();
            return json_encode(['status' => 200, 'mensaje' => "Se elimino correctamente" ]);
        }else{
            return json_encode(['status' => 200, 'mensaje' => "No se puede eliminar esta ocurrencia, dado que ya fue enviada." ]);
        }
    }
    public function get_ocurrencias( $request){
        $idadmin = $request['idadministrador'];
        $usuario = DB::table('administrador')->where('idadministrador','=',$idadmin)->first();
        $tam_pag = 100;
        if($usuario->idrol == 3){ // relab
            $ocurrencias = DB::table('registradorgerente_n')
                ->select('ocurrencias_n.*','ocurrencias_n.state as estado',DB::raw('case 
	when  datediff( now(), ocurrencias_n.created_at) < 2 then 0
	when  datediff( now(), ocurrencias_n.created_at) = 2 then 1
	when  datediff( now(), ocurrencias_n.created_at) = 3  then 1
	when  datediff( now(), ocurrencias_n.created_at) > 3 then 2
	end as prioridad'),'faltas_n.gravedad', 'faltas_n.falta', 'sanciones_n.sancion','afiliadoinfo_n.*')
                ->leftJoin('ocurrencias_n','registradorgerente_n.idregistrador','=','ocurrencias_n.idregistrador')
                ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
                ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
                ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
                ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
                ->whereIn('ocurrencias_n.state', [3])
                ->where('ocurrencias_n.idsancion','=',1)
                ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')->simplePaginate($tam_pag);
        }elseif($usuario->idrol == 1){ //registrador
            $ocurrencias = DB::table('ocurrencias_n')
                ->select('ocurrencias_n.state as estado',DB::raw('case 
	when  datediff( now(), ocurrencias_n.created_at) < 2 then 0
	when  datediff( now(), ocurrencias_n.created_at) = 2 then 1
	when  datediff( now(), ocurrencias_n.created_at) = 3  then 1
	when  datediff( now(), ocurrencias_n.created_at) > 3 then 2
	end as prioridad') , 'ocurrencias_n.*','afiliadoinfo_n.*',
                    'puestos_n.puesto','secciones_n.seccion','sanciones_n.sancion','sanciones_n.idsancion','faltas_n.idfalta','faltas_n.falta')
                ->Join('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
                ->Join('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
                ->Join('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
                ->Join('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
                ->Join('colaborador_registrador','afiliadoinfo_n.idafiliado','=','colaborador_registrador.idafiliado')
                ->Join('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')
                ->where('colaborador_registrador.idadministrador','=',$usuario->dni)
                ->orwhere('colaborador_registrador.idadministrador','=',$usuario->idadministrador)
                ->orderBy('ocurrencias_n.state', 'desc')
                ->groupBy('ocurrencias_n.idocurrencia')
                ->simplePaginate($tam_pag);
        }elseif($usuario->idrol == 4){ //super
            $ocurrencias = DB::table('ocurrencias_n')
                ->select('ocurrencias_n.state as estado',DB::raw('case 
	when  datediff( now(), ocurrencias_n.created_at) < 2 then 0
	when  datediff( now(), ocurrencias_n.created_at) = 2 then 1
	when  datediff( now(), ocurrencias_n.created_at) = 3  then 1
	when  datediff( now(), ocurrencias_n.created_at) > 3 then 2
	end as prioridad') , 'ocurrencias_n.*','afiliadoinfo_n.*',
                    'puestos_n.puesto','secciones_n.seccion','sanciones_n.sancion','sanciones_n.idsancion','faltas_n.idfalta','faltas_n.falta')
                ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
                ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
                ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
                ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
                ->leftJoin('colaborador_registrador','afiliadoinfo_n.idafiliado','=','colaborador_registrador.idafiliado')
                ->where('ocurrencias_n.state', '!=' , 7)
//                ->where('colaborador_registrador.idadministrador','=',$usuario->dni)
//                ->orwhere('colaborador_registrador.idadministrador','=',$usuario->idadministrador)
                ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')
                ->orderBy('ocurrencias_n.state', 'desc')
                ->groupBy('ocurrencias_n.idocurrencia')
                ->simplePaginate($tam_pag);
        }elseif($usuario->idrol == 2){ // gerencia
            $ocurrencias = DB::table('registradorgerente_n')
                ->select('ocurrencias_n.*','ocurrencias_n.state as estado'
                    ,DB::raw('case 
	when  datediff( now(), ocurrencias_n.created_at) < 2 then 0
	when  datediff( now(), ocurrencias_n.created_at) = 2 then 1
	when  datediff( now(), ocurrencias_n.created_at) = 3  then 1
	when  datediff( now(), ocurrencias_n.created_at) > 3 then 2
	end as prioridad'),'faltas_n.gravedad', 'faltas_n.falta', 'sanciones_n.sancion','afiliadoinfo_n.*')
                ->leftJoin('ocurrencias_n','registradorgerente_n.idregistrador','=','ocurrencias_n.idregistrador')
                ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
                ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
                ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')
                ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')
                ->where('registradorgerente_n.idgerente','=',$usuario->dni)
                ->whereIn('ocurrencias_n.state', [0, 2])
                ->leftJoin('secciones_n','afiliadoinfo_n.idseccion','=','secciones_n.idseccion')->simplePaginate($tam_pag);
        }
        foreach($ocurrencias as $ocurrencia){
            $ocurrencia->documentos = json_decode($ocurrencia->documentos);
        }
        return json_encode(array('status'=> 200, 'data' => array('ocurrencias' => $ocurrencias)));
    }
    public function upload_file( $request){
        $idocurrencia = $request['idocurrencia'];

        $file = $request->file('archivo');
        if ( !$request->hasFile('archivo') ||  !$file->isValid()) {
            return json_encode(array('status'=> 100, 'mensaje' => "Archivo invalido."));
        }

        if($file->getSize() > 50000000 ){
            return json_encode(array('status'=> 100, 'mensaje' => "El archivo es de ".$file->getSize() ." supera los 20 MB "));
        }
        switch ($file->extension()){
            case "jpeg":
                break;
            case "png":
                break;
            case "JPG":
                break;
            case "PNG":
                break;
            case "doc":
                break;
            case "docx":
                break;
            case "pdf":
                break;
            case "JPEG":
                break;
            case "DOC":
                break;
            case "jpg":
                break;
            default:
                return json_encode(array('status'=> 100, 'mensaje' => "El formato ".$file->extension() ." no es valido, los formatos son: png, jpg, jpeg, doc, docx, pdf "));
                break;
        }

        $ocurrencia = Ocurrencias::find($idocurrencia);
        if($ocurrencia == null){
            return json_encode(array('status'=> 100, 'mensaje' => "No existe esa ocurrencia."));
        }
        $lista_doc = new Estructuras\ListaDocumentos();
        $lista_doc->parse($ocurrencia->documentos,$idocurrencia);

        $time = time();
        $nombre = $time.'_'.$file->getClientOriginalName();
        $documento = new Estructuras\Documento($file->extension(),$idocurrencia, $nombre);
        array_push($lista_doc->lista,$documento);
        $file->move('ocurrencias/'.$idocurrencia, $nombre);
        $ocurrencia->documentos = json_encode($lista_doc->lista);
        $ocurrencia->save();

        return json_encode(array('status'=> 200, 'mensaje' => "Archivo Guardado Exitosamente."));
    }
    public function get_sancion($request){
        $idfalta = $request['idfalta'];
        $idafiliado = $request['idafiliado'];
        $reincidencia = $this->val_reincidencia($idafiliado);
        if($reincidencia > 2)$reincidencia = 2;

        $idsancion = DB::table('faltasancion_n')->where('idfalta','=',$idfalta)->where('cant_incidencias' , '=' , $reincidencia)->first();
        $sancion = Sanciones::find($idsancion->idsancion);

        return json_encode(['status'=>200, 'data'=>$sancion, 'reincidencia' => $reincidencia]);
    }
    public function val_reincidencia($idafiliado){
        $reincidencia = DB::select("select count(idocurrencia) as reincidencia from ocurrencias_n
                        where ocurrencias_n.fec_ocurrencia  >= DATE(NOW()) - INTERVAL 180 DAY and
                        ocurrencias_n.state = 7 and ocurrencias_n.idafiliado = ? ;", [$idafiliado]);
        return $reincidencia[0]->reincidencia;

    }
    /*********MODIFICARION RELAB-*******************/
    public function modificar($request){
        $idfuncionalidad = 4;
        $usuario = Auth::user();
        $fec_ini = $request['fec_suspencion_ini'];
        $fec_fin = $request['fec_suspencion_fin'];
        $ocurrencia = Ocurrencias::where('idocurrencia','=',$request['idocurrencia'])->first();
        $ocurrencia->fec_suspencion_ini = $fec_ini;
        $ocurrencia->fec_suspencion_fin = $fec_fin;
        $ocurrencia->save();

        return json_encode(['status' => 200, 'mensaje' => 'Se modifico satisfactoriamente']);
    }
    /**********RASTREO****************************/
    public function rastreo( $request){
        $ocurrencia = $request['idocurrencia'];
        $logs = DB::table('logsanciones_n')
            ->select('ocurrencias_n.idocurrencia','logsanciones_n.descripcion','logsanciones_n.created_at','logsanciones_n.tipo',
                'logsanciones_n.estado_ocurrencia','logsanciones_n.idcolaborador','logsanciones_n.idaprobador')
            ->join('ocurrencias_n','logsanciones_n.idocurrencia','=','ocurrencias_n.idocurrencia')
            ->where('logsanciones_n.idocurrencia','=',$ocurrencia)->get();
        $tracer = [];
        foreach ($logs as $log){
            $rastreo = new Estructuras\RastreoOcurrencia($log->tipo,$log->estado_ocurrencia);
            $rastreo->idocurrencia = $log->idocurrencia;
            if($log->descripcion == null){
                $rastreo->detalle = " -- ";
            }else{
                $rastreo->detalle = $log->descripcion;
            }

            $rastreo->dni_aprobador = $log->idaprobador;
            $rastreo->dni_colaborador = $log->idcolaborador;
            $rastreo->fecha_creacion = $log->created_at;
            $tracer []= $rastreo;
        }
        return json_encode(['status' => 200, 'data' => $tracer]);


    }

    public function validar($estado){


    }






}
