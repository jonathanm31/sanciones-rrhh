<?php

namespace App\Http\Controllers\Sanciones;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Models\Administrador;
use App\Models\Faltas;
use App\Http\Controllers\Controller;

class FaltaController extends Controller
{

    public function index(Request $request){
        return view('Sanciones.faltas');
    }
    public function get_all(Request $request){
        $faltas = DB::table('faltas_n')->get();
        return json_encode(array('status'=> 200, 'data' => $faltas));
    }
    public function validar_login(Request $request){
        $token = $request['token'];
        $idadmin = $request['idadministrador'];
        $usuario = Administrador::where('token','=',$token)->where('idadministrador','=',$idadmin)->first();
        if($usuario){
            return true;
        }else{
            return false;
        }

    }
    public function get_faltas(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);
        $faltas = DB::table('faltas_n')->get();
        return json_encode(array('status'=> 200, 'data' => $faltas));
    }
    public function get_sanciones(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);
        $sanciones = DB::table('sanciones_n')->get();
        return json_encode(array('status'=> 200, 'data' => $sanciones));
    }
    public function get_calificaciones(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);
        $idcolaborador = $request['idcolaborador'];
        $usuario = DB::table('afiliadoinfo_n')->where('idafiliado','=',$idcolaborador)->first();
        $current = '2018-07-19';
        $diff = abs(strtotime($usuario->fec_contratacion) - strtotime($current));
        $years = $diff / (365*60*60*24);
        if($years < 1.00) return json_encode(['status' => 100, 'mensaje' => 'Tienes que tener al menos 1 a単o en la empresa para ingresar en sanciones.']);
        $calificaciones = DB::table('calificaciones_n')->get();
        return json_encode(array('status'=> 200, 'data' => $calificaciones));
    }
    public function insertar(Request $request){
        $categoria = $request['categoria'];
        $gravedad = $request['gravedad'];
        $falta_data = $request['nombre'];
        $state = $request['estado'];
        $falta = new Faltas;
        $falta->idfaltacategoria = $categoria;
        $falta->gravedad = $gravedad;
        $falta->falta = $falta_data;
        $falta->state = $state;
        $falta->save();
        return json_encode(array('state'=>200,'mensaje'=> 'Se Guardo Correctamente'));
    }
    public function modificar(Request $request){
        $categoria = $request['categoria'];
        $gravedad = $request['gravedad'];
        $falta_data = $request['nombre'];
        $state = $request['estado'];
        $falta = Faltas::find($request['idfalta']);
        $falta->idfaltacategoria = $categoria;
        $falta->gravedad = $gravedad;
        $falta->falta = $falta_data;
        $falta->state = $state;
        $falta->save();
        return json_encode(array('state'=>200,'mensaje'=> 'Se modifico Correctamente'));

    }
    public function eliminar(Request $request){
        $falta = Faltas::find($request['idfalta']);
        $falta->delete();
        return json_encode(array('state'=>200,'mensaje'=> 'Se elimino Correctamente'));
    }
}
