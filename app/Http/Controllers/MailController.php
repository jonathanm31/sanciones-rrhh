<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class MailController extends Controller
{
    public function __construct()
    {

    }
    public $server ='https://app.mapsalud.com/clientes/somosoh/services/public/';

    protected function getServiciosInfo($servicio,$data){
        $url = $this->server.$servicio;
        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) {
            return Redirect::to('login')->with('error','Error de servidor');
        }else{
            return $result;
        }
    }
    public function mensaje(Request $request){
        $data = array("idrol" => 0,'idmessage' => $request['id']);
        $result= $this->getServiciosInfo('get_message',$data);
        return view('mensajes.mail')->with('mensaje' , json_decode($result));
    }
    public function aprobar(Request $request){
        $data = array("idrol" =>0,'idmessage' => $request['idmessage']);
        $result= $this->getServiciosInfo('approve_message',$data);
        return view('mensajes.mailmensaje')->with("mensaje", json_decode($result));
    }
    public function denegar(Request $request){
        $data = array("idrol" => 0,'idmessage' => $request['idmessage'],'reason' => $request['razon']);
        $result= $this->getServiciosInfo('deny_message',$data);
        return $result;
    }
    public function cancelar(Request $request){
        $data = array("idrol" =>0,'idmessage' => $request['idmessage']);
        $result= $this->getServiciosInfo('cancel_cupon',$data);
        return view('mensajes.mailmensajedenegado')->with("mensaje", json_decode($result));
    }

}
