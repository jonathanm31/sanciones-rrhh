<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'uploadfile_ocurrencias','ocurrencias_filtro','crear_ocurrencia','pedir_modificacion',
        'enviar_modificacion','aceptado_gerencia','denegado_gerencia','aceptado_relab',
        'denegado_relab','cerrado',"get_ocurrencia","rastreo","get_sancion",
        'modificar_ocurrencia','enviar_gerencia','get_faltas_query','eliminar_ocurrencia',
        'authentication','get_afiliados','get_faltas','get_sanciones','get_calificaciones','get_ocurrencias','get_formato_html','Service_formato'
    ];
}
