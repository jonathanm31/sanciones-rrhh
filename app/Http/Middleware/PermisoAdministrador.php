<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PermisoAdministrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(! Auth::user()) return redirect('login');

        $user = Auth::user()->idrol;

        switch ($user) {
            case '0':
                return redirect('denied');
                break;
            case '1':
//              Es un registrador
                break;
            case '2':
//              Es un administrador
                break;
            case '3':
//              Es un relab
                break;
            case '4':
//              Es un supes admin
                break;
            default:
                return redirect('login');
                break;
        }

        return $next($request);
    }
}
