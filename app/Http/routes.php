<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if(Auth::guest()){
        return redirect()->guest('login');
    }
    return redirect('home');
});



Route::get('denied',function(){
   return view('auth.permisos');
});
$this->get('logout', 'Auth\AuthController@logout');

Route::group(['middleware' => 'guest_group'],function(){
    $this->get('login', 'Auth\AuthController@showLoginForm');
    $this->post('login', 'Auth\AuthController@login');
    $this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    $this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    $this->post('password/reset', 'Auth\PasswordController@reset');
});

Route::group(['middleware' => 'admin_group'],function(){

   /******SANCIONES***************/
    $this->get('home','Sanciones\ReportesController@home');

    $this->get('faltas','Sanciones\FaltaController@index');
    $this->post('faltas_all','Sanciones\FaltaController@get_all');
    $this->post('insertar_falta','Sanciones\FaltaController@insertar');
    $this->post('modificar_falta','Sanciones\FaltaController@modificar');
    $this->post('eliminar_falta','Sanciones\FaltaController@eliminar');

    $this->get('sanciones','Sanciones\SancionesController@index');
    $this->post('sanciones_all','Sanciones\SancionesController@get_all');
    $this->post('insertar_sanciones','Sanciones\SancionesController@insertar');
    $this->post('modificar_sanciones','Sanciones\SancionesController@modificar');
    $this->post('eliminar_saciones','Sanciones\SancionesController@eliminar');

    $this->post('qrastreo','Sanciones\OcurrenciasController@rastreo');

    $this->get('lista_ocurrencia','Sanciones\OcurrenciasController@index')->name('listas');
    $this->get('nueva_ocurrencia','Sanciones\OcurrenciasController@nueva_ocurrencia')->name('nuevo');
    $this->post('ocurrencias_all','Sanciones\OcurrenciasController@get_all')->name('lista_ocurrencias');
    $this->post('qget_sanciones', 'Sanciones\OcurrenciasController@get_sancion');
    $this->post('qget_faltas', 'Sanciones\OcurrenciasController@get_faltas');
    $this->post('qget_colaborador', 'Sanciones\OcurrenciasController@get_colaborador');
    $this->get('editar_ocurrencia','Sanciones\OcurrenciasController@editar_ocurrencia')->name('editar_ocurrencias');
    /***********    ESTADOOOOSSS*****************/
    $this->post('qenviar_ocurrencia','Sanciones\OcurrenciasController@qenviar')->name('qenviar_ocurrencias');

    $this->post('qenviar_modifiacion','Sanciones\OcurrenciasController@qenviarmodifiacion')->name('qenviarmodifiacion');
    $this->post('qpedir_modificacion','Sanciones\OcurrenciasController@qpedirmodificacion')->name('qpedir_modificacion');

    $this->post('qaceptar_gerencia','Sanciones\OcurrenciasController@qaceptado')->name('qaceptado');
    $this->post('qdenegado_gerencia','Sanciones\OcurrenciasController@qdenegado')->name('qdenegado');
    $this->post('qaceptado_relab','Sanciones\OcurrenciasController@qaceptadorelab')->name('qaceptadorelab');
    $this->post('qdenegado_relab','Sanciones\OcurrenciasController@qdenegadorelab')->name('qdenegadorelab');

    $this->post('qcerrar','Sanciones\OcurrenciasController@qcerrar')->name('qcerrar');

    $this->post('qinsertar_ocurrencia','Sanciones\OcurrenciasController@qinsertar')->name('qinsertar_ocurrencia');
    $this->post('qmodificar_ocurrencia','Sanciones\OcurrenciasController@qmodificar')->name('qmodificar_ocurrencia');
    $this->post('qeliminar_ocurrencia','Sanciones\OcurrenciasController@qeliminar')->name('qeliminar_ocurrencia');
    $this->post('quploadfile_ocurrencias' , 'Sanciones\OcurrenciasController@qupload_file')->name('quploadfile_ocurrencia');
    $this->get('file','Sanciones\ServiciosController@get_file')->name('qarchivo_pdf');

    $this->get('val_ocurrencia','Sanciones\ServiciosController@val_reincidencia');

    /********REPORTES*******************/
    $this->get('get_repo1','Sanciones\ReportesController@get_prom_faltas');
    $this->get('get_repo2','Sanciones\ReportesController@get_prom_sanciones');
    $this->get('get_repo3','Sanciones\ReportesController@get_frec_areas');
    $this->get('get_repo4','Sanciones\ReportesController@get_frec_ocurrencias');


});
$this->post('authentication','Sanciones\ServiciosController@auntenticacion');

$this->post('get_afiliados','Sanciones\ServiciosController@afiliado_info');
$this->post('get_faltas','Sanciones\FaltaController@get_faltas');
$this->post('get_sanciones','Sanciones\FaltaController@get_sanciones');


$this->post('get_faltas_query','Sanciones\ServiciosController@search_faltas');

/******FLUJO****************/
$this->post('enviar_gerencia','Sanciones\ServiciosController@enviar_gerencia');
$this->post('pedir_modificacion','Sanciones\ServiciosController@pedir_modificacion');
$this->post('enviar_modificacion','Sanciones\ServiciosController@enviar_modificacion');
$this->post('aceptado_gerencia','Sanciones\ServiciosController@aceptado_gerencia');
$this->post('denegado_gerencia','Sanciones\ServiciosController@denegado_gerencia');
$this->post('denegado_relab','Sanciones\ServiciosController@denegado_relab');
$this->post('aceptado_relab','Sanciones\ServiciosController@aceptado_relab');
$this->post('cerrado','Sanciones\ServiciosController@cerrado');

$this->post('get_ocurrencias','Sanciones\ServiciosController@get_ocurrencias');
$this->post('get_ocurrencia','Sanciones\ServiciosController@get_ocurrencia');
$this->post('eliminar_ocurrencia','Sanciones\ServiciosController@eliminar_ocurrencia');
$this->post('crear_ocurrencia','Sanciones\ServiciosController@insertar');
$this->post('uploadfile_ocurrencias' , 'Sanciones\ServiciosController@upload_file');
$this->post('rastreo' , 'Sanciones\ServiciosController@rastreo');
$this->post('get_sancion', 'Sanciones\ServiciosController@get_sancion');

$this->get('get_formato','Sanciones\ServiciosController@get_file');
$this->post('get_formato_html','Sanciones\ServiciosController@get_file_html');
$this->post('Service_formato','Sanciones\ServiciosController@servicio_file');

$this->post('ocurrencias_filtro','Sanciones\OcurrenciasController@get_all_filtro');
$this->post('modificar_ocurrencia','Sanciones\OcurrenciasController@modificar');
