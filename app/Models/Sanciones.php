<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sanciones extends Model
{
    protected $table = "sanciones_n";
    protected $primaryKey = "idsancion";
    protected $timestamp = "false";
}
