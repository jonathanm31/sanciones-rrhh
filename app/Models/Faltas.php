<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faltas extends Model
{
    protected $table = "faltas_n";
    protected $primaryKey = "idfalta";
}
