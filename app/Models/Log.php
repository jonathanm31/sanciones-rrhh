<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = "logsanciones_n";
    protected $primaryKey = "idlogsancion";
}
