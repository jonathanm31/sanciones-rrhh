
function sidenav () {

    function toggle () {
        if($('.sidenav').css('width') == '0px'){
            // open
     
            $('.sidenav').css({
                "width" : "60px"
            });
        
            $('.admin-content').css({
                "margin-left" : "60px"
            });
        
        } else {
            // close
    
            $('.sidenav').css({
                "width" : 0
            });
        
            $('.admin-content').css({
                "margin-left" : 0
            });    
    
        }    
    }


    $('.sidenav-trigger').click(e => {
        e.preventDefault();
        toggle();
    });

}

sidenav();