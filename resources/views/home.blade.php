@extends('layouts.app')
@section('breadcrumb')
    Panel de Control - Reportes
@endsection
@section('content')
<div class="container">
    <div class="row">
        @if($reportes[0]['finantienda'])
        <h2>Reporte del {{ $reportes[0]['finantienda'][0]->date_1 }} al {{ $reportes[0]['finantienda'][0]->date_2 }} </h2>
        @endif
    </div>
    <div class="row">
          <div class="col-md-4 col-xd-12">
              <div class="panel panel-flat">
                  <div class="panel-heading">
                      <h6 class="panel-title text-semibold"># de Ocurrencias Central<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                  </div>
                  <div class="panel-body">
                      <div class="chart-container">
                          <canvas id="ocurrencias_central" width="100%" height="100"></canvas>
                      </div>
                  </div>
              </div>
          </div>
        <div class="col-md-4 col-xd-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold"># de Ocurrencias Finantienda <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                </div>
                <div class="panel-body">
                    <div class="chart-container">
                        <canvas id="ocurrencias_finantienda" width="100%" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xd-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Faltas mas Ocurridas Central <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                </div>
                <div class="panel-body">
                    <div class="chart-container">
                        <canvas id="faltas_central" width="100%" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xd-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Faltas mas Ocurridas Finantienda <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                </div>
                <div class="panel-body">
                    <div class="chart-container">
                        <canvas id="faltas_finantienda" width="100%" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xd-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Sanciones mas Ocurridas Central <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                </div>
                <div class="panel-body">
                    <div class="chart-container">
                        <canvas id="sanciones_central" width="100%" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xd-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Sanciones mas Ocurridas Finantienda <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                </div>
                <div class="panel-body">
                    <div class="chart-container">
                        <canvas id="sanciones_finantienda" width="100%" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xd-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Ocurrencias por Areas en Central<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-columned">
                            <thead>
                            <tr>
                                <th>Frecuencia</th>
                                <th>Área</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($reportes[2]['central'] as $area)
                                    <tr>
                                    <td>{{ $area->frecuencia }}</td>
                                    <td>{{ $area->area }}</td></tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xd-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Ocurrencias por Areas en  Finantienda <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-columned">
                            <thead>
                            <tr>
                                <th>Frecuencia</th>
                                <th>Área</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reportes[2]['finantienda'] as $area)
                                <tr>
                                    <td>{{ $area->frecuencia }}</td>
                                    <td>{{ $area->area }}</td></tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="application/javascript">
    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }
    function getRandomRgb() {
        var num = Math.round(0xffffff * Math.random());
        var r = num >> 16;
        var g = num >> 8 & 255;
        var b = num & 255;
        return rgbToHex(r,g,b);
    }
    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }
    /*******ocurrencias******/
    var ocurrencia_central = {!! json_encode($reportes[3]['central'])  !!};
    var ocurrencia_finantienda = {!! json_encode($reportes[3]['finantienda'])  !!};

    var data_central = [];
    var label_central = [];
    var color_b = [];
    $.each(ocurrencia_central ,function(index, value){
        data_central.push( parseInt(value.frecuencia));
        label_central.push(value.fec_ocurrencia);
        color_b.push(getRandomRgb());
    });

    var data_stats;
    var ctx_S = document.getElementById("ocurrencias_central").getContext('2d');
    var myChart = new Chart(ctx_S, {
        type: 'line',
        data: {
            labels: label_central,
            datasets: [{
                label: '# de ocurrencias',
                data: data_central,
                backgroundColor: color_b[1]+'80',
                borderColor: color_b[1],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var data_finantienda = [];
    var label_finantienda = [];
    var color_a = [];
    $.each(ocurrencia_finantienda ,function(index, value){
        data_finantienda.push( parseInt(value.frecuencia));
        label_finantienda.push(value.fec_ocurrencia);
        color_a.push(getRandomRgb());
    });

    var ctx_F = document.getElementById("ocurrencias_finantienda").getContext('2d');
    var myChart = new Chart(ctx_F, {
        type: 'line',
        data: {
            labels: label_finantienda,
            datasets: [{
                label: '# de ocurrencias',
                data: data_finantienda,
                backgroundColor: color_a[0]+'80',
                borderColor: color_a[0],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
/******FALTAS*************/
    var faltas_central = {!! json_encode($reportes[0]['central'])  !!};
    var faltas_finantienda = {!! json_encode($reportes[0]['finantienda'])  !!};

    var data_central = [];
    var label_central = [];
    var color_b = [];
    $.each(faltas_central ,function(index, value){
        data_central.push( parseInt(value.contando));
        label_central.push(value.falta);
        color_b.push(getRandomRgb());
    });

    var data_stats;
    var ctx_Sa = document.getElementById("faltas_central").getContext('2d');
    var myChart = new Chart(ctx_Sa, {
        type: 'horizontalBar',
        data: {
            labels: label_central,
            datasets: [{
                label: '# de Faltas más Ocurridas',
                data: data_central,
                backgroundColor: color_b[1]+'80',
                borderColor: color_b[1],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var data_finantienda = [];
    var label_finantienda = [];
    var color_a = [];
    $.each(faltas_finantienda ,function(index, value){
        data_finantienda.push( parseInt(value.contando));
        label_finantienda.push(value.falta);
        color_a.push(getRandomRgb());
    });

    var ctx_Fa = document.getElementById("faltas_finantienda").getContext('2d');
    var myChart = new Chart(ctx_Fa, {
        type: 'horizontalBar',
        data: {
            labels: label_finantienda,
            datasets: [{
                label: '# de Faltas más Ocurridas',
                data: data_finantienda,
                backgroundColor: color_a[0]+'80',
                borderColor: color_a[0],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
/******Sanciones*************/
    var sanciones_central = {!! json_encode($reportes[1]['central'])  !!};
    var sanciones_finantienda = {!! json_encode($reportes[1]['finantienda'])  !!};

    var data_central = [];
    var label_central = [];
    var color_b = ['#ae1e23', '#f92c32','#fa565a','#f98d2c'];
    $.each(sanciones_central ,function(index, value){
        data_central.push( parseInt(value.contando));
        label_central.push(value.sancion);
        color_b.push(getRandomRgb());
    });

    var data_stats;
    var ctx_Ss = document.getElementById("sanciones_central").getContext('2d');
    var myChart = new Chart(ctx_Ss, {
        type: 'bar',
        data: {
            labels: label_central,
            datasets: [{
                label: '# de Sanciones más Ocurridas',
                data: data_central,
                backgroundColor: color_b,
                borderColor: color_b,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var data_finantienda = [];
    var label_finantienda = [];
    var color_a = ['#ae1e23', '#f92c32','#fa565a','#f98d2c'];
    $.each(sanciones_finantienda ,function(index, value){
        data_finantienda.push( parseInt(value.contando));
        label_finantienda.push(value.sancion);
    });

    var ctx_Fs = document.getElementById("sanciones_finantienda").getContext('2d');
    var myChart = new Chart(ctx_Fs, {
        type: 'bar',
        data: {
            labels: label_finantienda,
            datasets: [{
                label: '# de Sanciones más Ocurridas',
                data: data_finantienda,
                backgroundColor: color_a,
                borderColor: color_a,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });


</script>
@endsection
