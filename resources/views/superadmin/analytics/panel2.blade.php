@extends('layouts.app')

@section('content')
    <script type="application/javascript" src="{{ asset('javascript/Chart.js') }}">    </script>
<div class="container">
    <div class="row">
          <div class="col-md-6 col-xd-12">
              <div class="panel panel-flat">
                  <div class="panel-heading">
                      <h6 class="panel-title text-semibold">Analitica de Usuarios IOS<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                  </div>
                  <div class="panel-body">
                      <div class="stats-container">

                      </div>
                      <div class="chart-container">
                          <canvas id="statsbystate" width="100%" height="100"></canvas>
                      </div>
                  </div>
              </div>
          </div>
        <div class="col-md-6 col-xd-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Stats IOS<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                </div>
                <div class="panel-body">

                    <div class="panel-body">
                        <div class="table-responsible">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Pantallas</th>
                                    <th>vistas</th>
                                </tr>
                                </thead>
                                <tbody id="contenido-pantallas">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xd-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Stats IOS<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                </div>
                <div class="panel-body">
                    <div class="table-responsible">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Dispositivo</th>
                                <th>Promedio de duracion de Sessiones</th>
                                <th>Total Pantallas vistas</th>
                            </tr>
                            </thead>
                            <tbody id="contenido-dispositivos">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <script>
        $(document).ajaxSend(function(){
            $.LoadingOverlay("show", {
                fade  : [2000, 1000],
                zIndex          : 1080
            });
        });
        $(document).ajaxComplete(function(){
            $.LoadingOverlay("hide");
        });
        function getRandomRgb() {
            var num = Math.round(0xffffff * Math.random());
            var r = num >> 16;
            var g = num >> 8 & 255;
            var b = num & 255;
            return 'rgb(' + r + ', ' + g + ', ' + b + ')';
        }
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';

        var data_stats;
        $.get(currentLocation+'analyticusersios',function(data){
            obj = JSON.parse(data);
            console.log(obj);
            $('.stats-container').append("<h6>Usuarios nuevos: "+ obj.data[0][1] + "</h6>");
            $('.stats-container').append("<h6>Usuarios recurentes: "+ obj.data[1][1]  + "</h6>")
        });

        $.get(currentLocation+'analyticsusuariosios',function(data){
            data_stats = JSON.parse(data);
            console.log(data_stats);
            if(data_stats.status === 200){
                var datos_usuarios = [];
                var nuevos_usuarios = [];
                var fechas = [];
                $.each(data_stats.data,function(index,value){
                    var date = value[0].substring(0,4)+"-"+value[0].substring(5,6)+"-"+value[0].substring(6,8);
                    var dia = value[0].substring(6,8);
                    datos_usuarios.push(value[3]);
                    nuevos_usuarios.push(value[2]);
                    fechas.push(date);
                });



                var ctx_S = document.getElementById("statsbystate").getContext('2d');
                var data = {
                    labels: fechas,
                    datasets: [{
                        label: '# Numero de usuarios recurrentes ',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(225,0,0,0.4)",
                        borderColor: "red", // The main line color
                        borderCapStyle: 'square',
                        borderDash: [], // try [5, 15] for instance
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "black",
                        pointBackgroundColor: "white",
                        pointBorderWidth: 1,
                        pointHoverRadius: 8,
                        pointHoverBackgroundColor: "yellow",
                        pointHoverBorderColor: "brown",
                        pointHoverBorderWidth: 2,
                        pointRadius: 4,
                        pointHitRadius: 10,
                        // notice the gap in the data and the spanGaps: true
                        data: datos_usuarios,
                        spanGaps: true,
                    }, {
                        label: '# Numero de usuarios nuevos ',
                        fill: true,
                        lineTension: 0.1,
                        backgroundColor: "rgba(167,105,0,0.4)",
                        borderColor: "rgb(167, 105, 0)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "white",
                        pointBackgroundColor: "black",
                        pointBorderWidth: 1,
                        pointHoverRadius: 8,
                        pointHoverBackgroundColor: "brown",
                        pointHoverBorderColor: "yellow",
                        pointHoverBorderWidth: 2,
                        pointRadius: 4,
                        pointHitRadius: 10,
                        // notice the gap in the data and the spanGaps: false
                        data: nuevos_usuarios,
                        spanGaps: false,
                    }

                    ]
                };
                var linea1  = new Chart(ctx_S, {
                    type: 'line',
                    data: data,
                    options: { scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });

            }



        });
        $.get(currentLocation+'analyticdispositivosios',function(data) {
            data_stats = JSON.parse(data);
            console.log(data_stats);
            if (data_stats.status === 200) {
                var dispositivos = "";
                $.each(data_stats.data, function (index, value) {
                    dispositivos += "<tr><td>"+ value[0] +
                                    "</td><td>" + value[2] +
                                    "</td><td>" + value[3] +"</td></tr>";
                });
                $('#contenido-dispositivos').append(dispositivos);

            }
        });
        $.get(currentLocation+'analyticpantallasios',function(data) {
            data_stats = JSON.parse(data);
            console.log(data_stats);
            if (data_stats.status === 200) {
                var pantallas = "";
                $.each(data_stats.data, function (index, value) {
                    pantallas += "<tr><td>"+ value[0] +
                        "</td><td>" + value[1] +"</td></tr>";
                });
                $('#contenido-pantallas').append(pantallas);

            }
        });


    </script>
@endsection
