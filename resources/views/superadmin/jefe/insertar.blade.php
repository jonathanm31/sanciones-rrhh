@extends('layouts.app')
@section('breadcrumb')
    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <img class="icon" src="{{asset('images/icons/cupon.png')}}" alt="">
            <a href="/super_agregaradministrador" class="ico-cupon">Agregar Administrador</a></li>
    </ul>
@endsection
@section('content')
@if(Auth::user()->idrol == '1')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-4">
                                <h4>INFORMACIÓN DEL ADMINISTRADOR:</h4>
                            </div>

                            <div class="col-md-2 col-md-offset-6">
                                <div class="boton-editar">
                                    <button class="btn btn-info text-slate-800 btn-flat" id="nuevo" data-id="105">
                                    <span style="color:#009bdd">
                                        Nuevo Administrador
                                    </span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="panel-body">
                        <div id="div-alerta" class="row" >
                            <div class="alert alert-warning" role="alert" id="alerta"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tipo">Nombres</label>
                                    <div class="input-group">
                                        <input type="text" id="nombres" placeholder="Nombres del Administrador">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tipo">Apellidos</label>
                                    <div class="input-group">
                                        <input type="text" id="apellidos" placeholder="Apellidos del Administrador">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <div class="input-group">
                                        <input type="text" id="email" placeholder="Email@gmail.com">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <div class="input-group">
                                        <input type="text" id="password" placeholder="*********">
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
        $('#div-alerta').hide();

        function validar(correo){
            var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
            if(caract.test(correo) == false){
                $('#div-alerta').show();
                $('#alerta').html('').append("El email es incorrecto.");
                return false;
            }
            return true;
        }

        $("#nuevo").click(function () {
           var nombres = $("#nombres").val();
           var apellidos = $("#apellidos").val();
           var email = $("#email").val();
           var password = $("#password").val();

           if(validar(email)){
               $('#div-alerta').hide();
               var form = new FormData();
               form.append("nombres",nombres);
               form.append("apellidos",apellidos);
               form.append("email",email);
               form.append("password",password);
               $.ajax({
                   url : "nuevoadministrador",
                   type: "POST",
                   data : form,
                   processData: false,
                   contentType: false,
                   success:function(data, textStatus, jqXHR){
                       swal({title: "Listo", text: "Se agrego un administrador exitosamente."},
                           function(){
                               window.location.href = currentLocation+"administradores";
                           }
                       );
                   },
                   error: function(jqXHR, textStatus, errorThrown){
                       //if fails
                   }
               });
           }


        });



    </script>
@endif
@endsection
