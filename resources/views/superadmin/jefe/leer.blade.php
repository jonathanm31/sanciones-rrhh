@extends('layouts.app')
@section('breadcrumb')
    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <img class="icon" src="{{asset('images/icons/cupon.png')}}" alt="">
            <a href="/super_administradores" class="ico-cupon">Administradores</a></li>
    </ul>
@endsection
@section('content')
@if(Auth::user()->idrol == '1')
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="container">
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Rol</th>
                    <th>Acciones</th>

                </tr>
                </thead>
                <tbody id="cuerpo">
                @forelse($administradores->data as  $administrador)
                    <tr>
                        <td>
                         {{ $administrador->nombres  }}
                        </td>
                        <td>
                            {{ $administrador->apellidos  }}
                        </td>
                        <td>
                            <?php
                                if($administrador->idrol == 0){
                                    echo "administrador";
                                }else{
                                    echo "super administrador";
                                }

                            ?>
                        </td>
                        <td id="data-delete">
                            <button type="button" class="btn btn-danger" id="eliminar" data-id="{{ $administrador->idadministrador  }}">Eliminar</button>
                        </td>


                    </tr>
                @empty
                    <div>No hay administradores.</div>
                @endforelse

                </tbody>
            </table>
        </div>

    </div>
    </div>


    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';

        $("#cuerpo").on('click','td #eliminar',function(){
            var idadministrador = $(this).data("id");

            swal({
                title: "¿Está seguro que deseas eliminar.",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "CONFIRMAR",
                closeOnConfirm: false,
                closeOnCancel: false
            },function(isConfirm) {
                if (isConfirm) {
                    var razon = $('#razon').val();
                    $.post(currentLocation+"eliminaradmin",{ "idadministrador":idadministrador }, function(){
                        swal({title: "Listo", text: "Se eliminó un administrador exitosamente."},
                            function(){
                                location.reload();
                            }
                        );
                    });
                } else {
                    swal("Ok, saliste!");

                }
            });

            



        })
    </script>

@endif
@endsection
