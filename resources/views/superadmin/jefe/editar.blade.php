@extends('layouts.app')
@section('breadcrumb')
    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <img class="icon" src="{{asset('images/icons/cupon.png')}}" alt="">
            <a href="/super_editaradministrador" class="ico-cupon">Editar Administrador</a></li>
    </ul>
@endsection
@section('content')
@if(Auth::user()->idrol == '1')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-4">
                                <h4>INFORMACIÓN DEL ADMINISTRADOR:</h4>
                            </div>

                            <div class="col-md-2 col-md-offset-6">
                                <div class="boton-editar">
                                    <button class="btn btn-info text-slate-800 btn-flat" id="editar" data-id="105">
                                    <span style="color:#009bdd">
                                        Nuevo Administrador
                                    </span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tipo">Nombres</label>
                                    <div class="input-group">
                                        <input type="text" id="nombre" placeholder="Nombres del Administrador">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tipo">Apellidos</label>
                                    <div class="input-group">
                                        <input type="text" id="apellidos" placeholder="Apellidos del Administrador">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tipo">Email</label>
                                    <div class="input-group">
                                        <input type="text" id="email" placeholder="Email@gmail.com">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tipo">Password</label>
                                    <div class="input-group">
                                        <input type="text" id="email" placeholder="*********">
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
