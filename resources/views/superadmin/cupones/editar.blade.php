@extends('layouts.app')
@section('breadcrumb')
    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <img class="icon" src="{{asset('images/icons/cupon.png')}}" alt="">
            <a href="/super_cupones" class="ico-cupon">Editar Cupon</a></li>
    </ul>
@endsection
@section('content')
@if(Auth::user()->idrol == '1')
    <?php
    $data = $cupon->data[0];
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-4">
                                <h4>INFORMACIÓN DEL CUPÓN:</h4>
                            </div>

                            <div class="col-md-2 col-md-offset-6">
                                <div class="boton-editar">
                                    <button class="btn btn-info text-slate-800 btn-flat" id="editar" data-id="105">
                                    <span style="color:#009bdd">
                                        Guardar Cambios
                                    </span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="tipo"></label>
                                    <div class="input-group">
                                        <select id="tipo" type="text" class="form-control" name="tipo" placeholder="DNI COLABORADOR">
                                            <option value="1">Central</option>
                                            <option value="2">Finantiendas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="margin-bottom: 20px">
                            <div class="col-md-2 text-center" style="vertical-align:middle;     display:inline-block;" >
                                <div id="btn-img" style="width: 150px; height: 80px; padding: 2px; ">
                                    <div id="cont-img" style="background-color: #f5f5f5; width: 150px;  height: 75px ; ">
                                        <img class="img-circle img-center" id="preview" width="70" height="70" src="https://app.mapsalud.com/clientes/somosoh/admin/upload/files/{{ $data->imagen  }}" alt="">
                                    </div>
                                    <div id="img-load" style="background-color: #5b5c61; color:#fff;position: absolute;width: 150px; z-index: 99999">
                                        <small>CAMBIAR IMAGEN</small>
                                    </div>
                                </div>
                                <input type="file" id="imagen" style="display:none">

                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <h6>Color:</h6>
                                <input class="form-control colorpicker-show-input" data-preferred-format="rgb" id="color" value="{{ $data->color }}" type="text">
                            </div>
                            <div class="col-md-12">
                                <h6>Nombre del cupón:</h6>
                                <input type="text" id="nombre" name="nombre" value="{{ $data->nombre }}" >
                                <input type="hidden" id="idcupon" name="idcupon" value="{{ $data->idcupon }}" >
                            </div>
                            <div class="col-md-12">
                                <h6>Titulo del cupón:</h6>
                                <input type="text" width="150" id="titulo" name="titulo" value="{{ $data->titulo }}" >
                            </div>
                            <div class="col-md-12">
                                <h6>Estado:</h6>
                                <div class="input-group">
                                    <select id="estado" type="text" class="form-control" name="tipo">
                                        <option value="0">ACTIVO</option>
                                        <option value="1">INACTIVO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h6>Cantidad de usos del cupón:</h6>
                                <input type="number" id="cantidad" name="cantidad" value="{{ $data->cantidad_afiliado}}" >
                            </div>

                            <div class="col-md-12">
                                <h6>Mensaje:</h6>
                                <textarea name="texto" id="texto" cols="150" rows="8">{{$data->texto}}
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript">
        var estado = "{{ $data->state }}";
        var tipo = "{{ $data->idcentral }}"

        $("#tipo").val(tipo);
        $("#estado").val(estado);


        $("#btn-img").click(function(){
           $("#imagen").trigger('click');
        });

        $("#imagen").change(function(){
            var imagen = $(this).prop('files');
            var reader = new FileReader();
            $("#preview").attr("title",imagen.name);

            reader.onload = function(event) {
                $("#preview").attr("src",event.target.result);
            };

            reader.readAsDataURL(imagen[0]);

        });
        $("#editar").click(function(event){
            var form = new FormData();
            var nombre = $("#nombre").val();
            var nom_img = "{{ $data->imagen }}";
            var idcupon = $("#idcupon").val();
            var titulo = $("#titulo").val();
            var color = $("#color").val();
            var cantidad = $("#cantidad").val();
            var estado = $("#estado").val();
            var tipo = $("#tipo").val();
            var texto = $("#texto").val();

            if( $("#imagen").get(0).files.length > 0){
                form.append("imagen",  $("#imagen")[0].files[0]);
            }else{
                form.append("nom_img", nom_img);
            }



            form.append("nombre",nombre);
            form.append("idcupon",idcupon);
            form.append("titulo",titulo);
            form.append("color",color);
            form.append("estado",estado);
            form.append("cantidad",cantidad);
            form.append("tipo",tipo);
            form.append("texto",texto);


            $.ajax({
                url : "cambioscupon",
                type: "POST",
                data : form,
                processData: false,
                contentType: false,
                success:function(data, textStatus, jqXHR){
                    swal({title: "Listo", text: "Se edito el cupon exitosamente."},
                        function(){
                            location.reload();
                        }
                    );
                },
                error: function(jqXHR, textStatus, errorThrown){
                    //if fails
                }
            });

        })
    </script>
@endif
@endsection
