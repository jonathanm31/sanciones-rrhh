@extends('layouts.app')
@section('breadcrumb')
    <a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
    <ul class="breadcrumb">
        <li>
            <img class="icon" src="{{asset('images/icons/cupon.png')}}" alt="">
            <a href="/super_cupones" class="ico-cupon">Nuevo Afiliado</a></li>
    </ul>
@endsection
@section('content')
@if(Auth::user()->idrol == '1')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-4">
                                <h4>INFORMACIÓN DEL AFILIADO:</h4>
                            </div>

                            <div class="col-md-2 col-md-offset-6">
                                <div class="boton-editar">
                                    <button class="btn btn-info text-slate-800 btn-flat" id="editar" data-id="105">
                                    <span style="color:#009bdd">
                                        Guardar Cambios
                                    </span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h6>Central:</h6>
                                <div class="form-group">
                                    <label for="tipo"></label>
                                    <div class="input-group">
                                        <select id="tipo" type="text" class="form-control" name="tipo" placeholder="DNI COLABORADOR">
                                            <option value="1">Central</option>
                                            <option value="2">Finantiendas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h6>Plan del Afiliado:</h6>
                                <div class="input-group">
                                    <select id="plan" type="text" class="form-control" name="tipo">
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6>DNI del Afiliado:</h6>
                                <input type="number" id="dni" name="dni" value="" >
                            </div>
                            <div class="col-md-12">
                                <h6>Nombres del Afiliado:</h6>
                                <input type="text" id="nombre" name="nombre" value="" >
                            </div>
                            <div class="col-md-12">
                                <h6>Apellidos Paterno:</h6>
                                <input type="text" id="apellido_paterno" name="apellido" value="" >
                            </div>
                            <div class="col-md-12">
                                <h6>Apellidos Materno:</h6>
                                <input type="text" id="apellido_materno" name="apellido" value="" >
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript">
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';

        $.get('planes',function(data){
            var obj = JSON.parse(data);
            var ops = "";
            $.each(obj.data,function(index,value){
               ops += "<option value='"+ value.idplan +"'>"+ value.plan+"</option>";
            });
            $('#plan').append(ops);
        });

        $("#editar").click(function(event){
            var form = new FormData();
            var nombre = $("#nombre").val();
            var apellidop = $("#apellido_paterno").val();
            var apellidom = $("#apellido_materno").val();

            var dni = $("#dni").val();
            var central = $("#tipo").val();
            var plan = $("#plan").val();

            form.append("nombres",nombre);
            form.append("apellido_paterno",apellidop);
            form.append("apellido_materno",apellidom);

            form.append("dni",dni);
            form.append("idcentral",central);
            form.append("idplan",plan);


            $.ajax({
                url : "nuevo_afiliado",
                type: "POST",
                data : form,
                processData: false,
                contentType: false,
                success:function(data, textStatus, jqXHR){
                    swal({title: "Listo", text: "Se agrego el cupon exitosamente."},
                        function(){
                            window.location.replace(currentLocation+"colaboradores?token="+dni);
                        }
                    );
                },
                error: function(jqXHR, textStatus, errorThrown){
                    //if fails
                }
            });

        })
    </script>
@endif
@endsection
