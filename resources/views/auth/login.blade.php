@extends('layouts.loginLayout')

@section('content')
    <style type="text/css">
        #email{
            background-color: rgba(11, 142, 231, 0.22);
        }
        #password{
            background-color: rgba(11, 142, 231, 0.22);

        }
        #logo{
            height: 20px !important;
            width: auto;
            text-align: center;
        }
        .content-logo{
            margin-bottom: 20px;
            margin-top: 50px;
        }
        b{
            color:#009bdd !important;
        }
    </style>
<div class="container">
                    <!-- Simple login form -->
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {!! csrf_field() !!}
                    <div class="panel panel-body login-form">
                    <div class="text-center">
                    <div class="content-logo">
                        <img id="logo" src="{{ asset('images/ic_oh.png') }}" class="img-responsive center-block" alt="">
                    </div>
                        <h5 class="content-group"><b>Bienvenido al Sistema de Sanciones!</b><small class="display-block">Ingresa a tu cuenta!</small></h5>
                    </div>

                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} has-feedback has-feedback-left">
                        <input id="email" type="email" class="form-control" placeholder="Correo Electronico" name="email" value="{{ old('email') }}">
                    {{--<div class="form-control-feedback">--}}
                    {{--<i class="icon-user text-muted"></i>--}}
                    {{--</div>--}}
                        @if ($errors->has('email'))
                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} has-feedback has-feedback-left">
                        <input id="password" placeholder="Contraseña" type="password" class="form-control" name="password">
                    {{--<div class="form-control-feedback">--}}
                    {{--<i class="icon-lock2 text-muted"></i>--}}
                    {{--</div>--}}
                        @if ($errors->has('password'))
                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">
                        INGRESAR
                    </button>
                    </div>

                    {{--<div class="text-center">--}}
                        {{--<a class="btn btn-link" href="{{ url('/password/reset') }}">Olvidaste tu password?</a>--}}
                    {{--</div>--}}
                    </div>
                    </form>




                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">--}}
                        {{--{{ csrf_field() }}--}}
                        {{--<div class="panel panel-body login-form">--}}
                            {{--<div class="panel-heading">Login</div>--}}
                            {{--<div class="panel-body">--}}
                                {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback has-feedback-left ">--}}
                                        {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">--}}
                                        {{--<div class="form-control-feedback">--}}
                                        {{--<i class="icon-user text-muted"></i>--}}
                                        {{--</div>--}}
                                        {{--@if ($errors->has('email'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                {{--</div>--}}

                                {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback has-feedback-left ">--}}
                                        {{--<input id="password" type="password" class="form-control" name="password">--}}
                                        {{--<div class="form-control-feedback">--}}
                                        {{--<i class="icon-lock2 text-muted"></i>--}}
                                        {{--</div>--}}
                                        {{--@if ($errors->has('password'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--<div class="checkbox">--}}
                                            {{--<label>--}}
                                                {{--<input type="checkbox" name="remember"> Remember Me--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<div class="col-md-6 col-md-offset-4">--}}
                                        {{--<button type="submit" class="btn btn-primary">--}}
                                            {{--<i class="fa fa-btn fa-sign-in"></i> Login--}}
                                        {{--</button>--}}

                                        {{--<a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
     </div>
@endsection
