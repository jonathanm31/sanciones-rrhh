<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sanciones</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/lista.css') }}" />
    <script src="{{ asset('assets/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('assets/js/loadingoverlay_progress.min.js') }}"></script>
    <style>
        .form-control-feedback {
            margin-top: 10px;
        }
        #logo-img{
            height: 10px;

        }
    </style>
    {!! csrf_field() !!}

</head>

<body class="login-container  pace-done"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div></div>

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="/">
            <img src="{{asset('images/Linicio.png')}}" class="img-responsive" alt="">
        </a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

    {{--<div class="navbar-collapse collapse" id="navbar-mobile">--}}
        {{--<ul class="nav navbar-nav navbar-right">--}}
            {{--<li>--}}
                {{--<a href="#">--}}
                    {{--<i class="icon-display4"></i> <span class="visible-xs-inline-block position-right"> Go to website</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            {{--<li>--}}
                {{--<a href="#">--}}
                    {{--<i class="icon-user-tie"></i> <span class="visible-xs-inline-block position-right"> Contact admin</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            {{--<li class="dropdown">--}}
                {{--<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--<i class="icon-cog3"></i>--}}
                    {{--<span class="visible-xs-inline-block position-right"> Options</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</div>--}}
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container" style="min-height:710px">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Simple login form -->
                {{--<form action="index.html">--}}
                    {{--<div class="panel panel-body login-form">--}}
                        {{--<div class="text-center">--}}
                            {{--<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>--}}
                            {{--<h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>--}}
                        {{--</div>--}}

                        {{--<div class="form-group has-feedback has-feedback-left">--}}
                            {{--<input type="text" class="form-control" placeholder="Username">--}}
                            {{--<div class="form-control-feedback">--}}
                                {{--<i class="icon-user text-muted"></i>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group has-feedback has-feedback-left">--}}
                            {{--<input type="password" class="form-control" placeholder="Password">--}}
                            {{--<div class="form-control-feedback">--}}
                                {{--<i class="icon-lock2 text-muted"></i>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>--}}
                        {{--</div>--}}

                        {{--<div class="text-center">--}}
                            {{--<a href="login_password_recover.html">Forgot password?</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</form>--}}
                <!-- /simple login form -->
                @if (Auth::guest())
                    @yield('content')
                @endif


                <!-- Footer -->
                <div class="footer text-muted text-center">
                    2017 <a href="#">desarrollado por Mapsalud</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->



</body></html>