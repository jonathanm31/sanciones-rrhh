<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ URL::to('/') }}">
    <title>Sistema de Sanciones</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>--}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

    <script src="{{ asset('assets/js/loadingoverlay.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/lista.css') }}" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


    <!-- /core JS files -->


    <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/color/spectrum.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/core/app.js') }}"></script>
    <!-- /theme JS files -->


</head>
<body class="sidebar-xs has-detached-left">

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

        </ul>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
        <a class="navbar-brand" href="/">
            <img src="{{asset('images/Linicio.png')}}">
        </a>


    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">

        @if (!Auth::guest())
        <ul class="nav navbar-nav navbar-right">
            <li class="">
                <a class="dropdown-toggle"href="logout">

                        <i class="icon-switch2"></i> Salir</a>


                {{--<ul class="dropdown-menu dropdown-menu-right">--}}
                    {{--<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>--}}
                    {{--<li><a href="#"><i class="icon-coins"></i> My balance</a></li>--}}
                    {{--<li><a href="#"><span class="badge badge-warning pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>--}}
                    {{--<li class="divider"></li>--}}
                    {{--<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>--}}
                    {{--<li><a href="/logout"><i class="icon-switch2"></i> Logout</a></li>--}}
                {{--</ul>--}}
            </li>
        </ul> @endif
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                {{--<div class="sidebar-user">--}}
                    {{--<div class="category-content">--}}
                        {{--<div class="media">--}}
                            {{--<a href="#" class="media-left"><img src="assets/images/image.png" class="img-circle img-sm" alt=""></a>--}}
                            {{--<div class="media-body">--}}
                                {{--<span class="media-heading text-semibold">Victoria Baker</span>--}}
                                {{--<div class="text-size-mini text-muted">--}}
                                    {{--<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="media-right media-middle">--}}
                                {{--<ul class="icons-list">--}}
                                    {{--<li>--}}
                                        {{--<a href="#"><i class="icon-cog3"></i></a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            {{--<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>--}}
                            @if (!Auth::guest())
                                <li class="{{ Request::is('/') ? 'class="start active open nav-item start"' : '' }}">
                                    <a href="{{ url('/') }}" class="nav-link nav-toggle">
                                        <img width="20" src="{{ Request::is('/') ? asset('images/icons/stats.png') : asset('images/icons/stats1.png') }}" alt="">
                                        <span class="title">Panel</span>
                                    </a>
                                </li>

                                @if(Auth::user()->idrol > 0)
                                    <li class="{{ Request::is('lista_ocurrencia') ? 'class="start active open nav-item start"' : '' }}">
                                        <a href="{{ route('listas') }}" class="nav-link nav-toggle">
                                            <img width="20" src="{{ Request::is('lista_ocurrencia') ? asset('images/icons/sanciones.png') : asset('images/icons/sanciones1.png') }}" alt="">
                                            <span class="title">Ocurrencias</span>
                                        </a>
                                    </li>
                                @endif

                                {{--@if(Auth::user()->idrol == '1' || Auth::user()->idrol == '0' )--}}
                                    {{--<li class="">--}}
                                        {{--<a href="{{ url('/user_panel') }}" class="nav-link nav-toggle">--}}
                                            {{--<i class="icon-diamond"></i>--}}
                                            {{--<span class="title">Usuario</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                            {{--@endif--}}


                        @endif
                       </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    {{--<div class="page-title">--}}
                        {{--@if (!Auth::guest())--}}
                            {{--@yield('titulo')--}}
                        {{--@endif--}}

                    {{--</div>--}}

                    <div class="heading-elements">
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        @if (!Auth::guest())
                            @yield('breadcrumb')
                        @endif
                    </ul>

                    <ul class="breadcrumb-elements">
                        @if (!Auth::guest())
                            @yield('menu')
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Ayuda</a></li>
                        @endif
                    </ul>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Detached content -->
                <div class="container-detached">

                        @yield('content')


                </div>
                <!-- /detached content -->


                <!-- Footer -->
                <div class="footer text-muted">
                    2018 <a href="#">desarrollado por Mapsalud</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
<script type="application/javascript">
    $(document).ajaxSend(function(){
        $.LoadingOverlay("show", {
            fade  : [500, 500],
            zIndex          : 1080
        });
    });
    $(document).ajaxComplete(function(){
        $.LoadingOverlay("hide");
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>




<script src="{{ asset('javascript/jquery.redirect.js') }}"></script>


</body>
</html>

