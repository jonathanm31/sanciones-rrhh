@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="assets/css/styles.css">

    <meta name="csrf-token" content="{{ Session::token() }}">

    <div class="row">
        <div class="col-md-6 ">
            <button class="btn btn-info" id="grabar">
                <i class="icon-floppy-disk"></i>
                Grabar
            </button>
            <button class="btn btn-info" id="subir" disabled>
                <i class="icon-floppy-disk"></i>
                Subir Arhivos
            </button>
            <button class="btn bg-success" id="btnEnviar" disabled>
                <i class="icon-arrow-up16"></i>
                Enviar
            </button>
            <a href="{{ route('listas') }}" class="btn btn-danger" id="atras" >
                <i class="icon-arrow-up-left2"></i> Salir
            </a>
        </div>
    </div>
    <div class="row div-row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-2">
                    <label for="">Buscar Colaborador:</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="colaborador" placeholder="Nombre, Apellido o DNI" >
                    <div class="list-colaboradores">
                    </div>
                </div>
            </div>
            <div  id="tarjeta" hidden>
                <div class="row">
                    <div class="col-md-4">
                        <h7>Documento de Identidad:</h7>
                    </div>
                    <div class="col-md-8" id="dni_col">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <h7>Nombres y Apellidos:</h7>
                    </div>
                    <div class="col-md-8" id="nom_col">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h7>Puesto:</h7>
                    </div>
                    <div class="col-md-8" id="puesto_col">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h7>Área:</h7>
                    </div>
                    <div class="col-md-8" id="area_col">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h7>F. Ingreso:</h7>
                    </div>
                    <div class="col-md-8" id="fecha_col">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h7>Sección:</h7>
                    </div>
                    <div class="col-md-8" id="seccion_col">
                    </div>
                </div>

            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Falta</label>
                </div>
                <div class="col-md-10">
                    {{--{{ dump($data["faltas"]) }}--}}
                    <select name="faltas" id="falta" class="form-control" disabled>
                        <option value="0" disabled selected>Escoger Falta</option>
                        @foreach($data["faltas"] as $falta)
                            <option value="{{ $falta->idfalta }}"> {{ $falta->falta }} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Sanción</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="sancion" placeholder="Sancion" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Reincidencia</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="reincidencia" placeholder="reincidencia" disabled>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Estado</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="estado" value="Borrador" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Fecha de Falta</label>
                </div>
                <div class="col-md-10">
                    <input type="text" data-date-format="yyyy-mm-dd" value="<?PHP echo  date("Y-m-d"); ?>" id="fec_ocurrencia" placeholder="Y-m-d" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Fecha de suspensión</label>
                </div>
                <div class="col-md-5">
                    <input type="text" data-date-format="yyyy-mm-dd" id="fec_ini" placeholder="Inicio de suspensión Y-m-d" disabled>
                </div>
                <div class="col-md-5">
                    <input type="text" data-date-format="yyyy-mm-dd"  id="fec_fin" placeholder="Fin de suspensión Y-m-d" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for="">Motivo de la Sanción</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group row">

                        <textarea type="text" id="descripcion"  style="width:100%;" ></textarea>
                        <p class="form-text text-muted  text-right">Solo tienes <b><span id="falta_count">150</span></b> caractetes</p>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12" >
            <div class="form-group row">
                <label for="archivos" class="col-sm-2 col-form-label">Cargar documentos:</label>
                <div class="col-sm-10">
                    <div class="file-loading border-teal">
                        <input id="archivos" class="file" type="file" multiple disabled>
                        <span class="form-text text-muted text-teal text-right">Debes grabar primero la ocurrencia </span> <br>
                        <span class="form-text text-muted text-danger text-right">Los archivos no pueden pesar mas de 5MB </span>

                        <input type="hidden" id="ocurrencia" class="border-teal" value="">
                    </div>
                    <div class="mensaje"></div>
                </div>
            </div>

        </div>

    </div>
    <div class="row div-row">
        <div class="row" id="doc-card">

        </div>
    </div>
    <script src="https://underscorejs.org/underscore.js"></script>

    <script type="application/javascript">

        $('#fec_ocurrencia').datepicker({
            startDate: '-1m',
            endDate: '+1d',
            datesDisabled: '+1d',
            daysOfWeekDisabled: [0]
        });
        $('#fec_ini').datepicker({
            daysOfWeekDisabled: [0,6]
        });
        $('#fec_fin').datepicker({
            daysOfWeekDisabled: [0,6]
        });


        var idfalta = 0;
        var idsancion = 0;
        var idcolaborador = 0;
        var ocurrencia;
        var colaboradores = [];

        /*********  FALTAS *******/
        $('#falta').on('change',function(){
            idfalta = $(this).val();
            console.log(idfalta);
            $.post('qget_sanciones',{idfalta:idfalta, idafiliado:idcolaborador},function(data){
                var sancion = JSON.parse(data);
                $('#sancion').val(sancion.data.sancion);
                idsancion = sancion.data.idsancion;
                $('#reincidencia').val(sancion.reincidencia);
                if(idsancion == 1){
                    $('#fec_ini').attr('disabled',false);
                    $('#fec_fin').attr('disabled',false);
                }else{
                    $('#fec_ini').attr('disabled',true);
                    $('#fec_fin').attr('disabled',true);
                }

            });
        });



        $('#colaborador').keyup(function(){
            var query = $(this).val();
            var idadmin = "<?PHP echo Auth::user()->idadministrador ; ?>";
            if(query.length >1 ){
                $('.list-colaboradores').html('');
                var item = "";
                $.post('qget_colaborador',{query:query, idadministrador:idadmin},function(data){
                    var colaborador = JSON.parse(data);
                    colaboradores = colaborador.data;
                    $.each(colaborador.data,function(index,value){
                        item += '<div class="item" data-id="'+ value.idafiliado +'">'+
                            value.nombres
                            +'</div>';
                    });
                    $('.list-colaboradores').html(item);
                });
            }else{
                idcolaborador = "";
                $('.list-colaboradores').html("");
                $('#tarjeta').hide();


            }
        });
        $('.list-colaboradores').on('click','.item',function(){
            $('#tarjeta').show();
            var texto = $(this).html();
            $('#colaborador').val(texto);
            idcolaborador = $(this).data('id');
            var colaborador = _.find(colaboradores, function(cb){ return cb.idafiliado == idcolaborador; });
            $('#dni_col').html(colaborador.idafiliado);
            $('#nom_col').html(colaborador.nombres);
            $('#puesto_col').html(colaborador.puesto);
            $('#area_col').html(colaborador.area);
            $('#seccion_col').html(colaborador.seccion);
            $('#fecha_col').html(colaborador.fec_contratacion);


            console.log(colaborador);

            $('.list-colaboradores').html('');
            $('#falta').attr('disabled',false);
        });
        var ocurrencia ;
        $('#grabar').click(function(){

            if(idcolaborador == "" || idfalta == ""){
                swal({
                    title: "Upps!",
                    text: "Falta rellenar el campo colaborador o falta.",
                    icon: "warning"
                });
            }else{
                swal({
                    title: "Guardar",
                    text: "Seguro que deseas guardar la ocurrencia?",
                    icon: "info",
                    buttons: [
                        'No, cancelar!',
                        'Si'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        var formData = new FormData();
                        formData.append('idcolaborador',idcolaborador);
                        formData.append('sancion',idsancion);
                        formData.append('falta',idfalta);
                        formData.append('reincidencia', $('#reincidencia').val());
                        formData.append('descripcion', $('#descripcion').val());


                        formData.append('fecha',$('#fec_ocurrencia').val());
                        formData.append('inicio',$('#fec_ini').val());
                        formData.append('fin',$('#fec_fin').val());
                        console.log(formData);
                        $.ajax({
                            url: '{{ route('qinsertar_ocurrencia') }}',
                            data: formData,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function(data){
                                ocurrencia = JSON.parse(data);
                                console.log(ocurrencia);
                                $('#ocurrencia').val(ocurrencia.data.idocurrencia);
                                $('#grabar').attr('disabled',true);
                                $('#subir').attr('disabled',false);
                                $('#btnEnviar').attr('disabled',false);
                                $('#archivos').attr('disabled',false);
                                $('#reincidencia').attr('disabled',true);
                                $('#descripcion').attr('disabled',true);
                                $('#fec_ocurrencia').attr('disabled',true);
                                $('#fec_ini').attr('disabled',true);
                                $('#fec_fin').attr('disabled',true);
                                $('#falta').attr('disabled',true);
                                $('#colaborador').attr('disabled',true);


                            }

                        });
                        swal({
                            title: 'Listo!',
                            text: 'Se guardo! ya puedes subir los documentos',
                            icon: 'success'
                        }).then(function() {
                            $('#mensaje').html('<div class="alert alert-info alert-styled-left alert-dismissible">' +
                                '<button type="button" class="close" data-dismiss="alert"><span>×</span></button>' +
                                'Ya puedes subir archivos: jpg, png, pdf, doc, excel ' +
                                '    </div>')
                        });
                    } else {
                        swal({
                            title: 'Upps!',
                            text: 'No se logro guardar la ocurrencia!',
                            icon: 'warning'
                        }).then(function() {
                            //window.location.href = "/lista_ocurrencia";
                        });
                    }
                })
            }
        });
        var files = new FormData();
        var files_array = [];


        $("#archivos").change(function(){
            var archivos = $(this).prop("files");
            var idocurrencia = 5;
            var card = "";

            for (var i = 0; i < archivos.length; i++) {
                var FileSize = archivos[i].size / 1024 / 1024; // in MB
                if (FileSize > 5) {
                    swal({
                        title: 'Upps!',
                        text: "El archivo "+ archivos[i].name +" es muy grande" ,
                        icon: 'warning'
                    })
                } else {
                    files_array.push(archivos[i]);
                    var index = files_array.indexOf(archivos[i]);
                    card += '            <div class="col-md-2" > ' +
                        '                <div class="card">' +
                        '                    <div class="card-body">' +
                        '                        <div class="row">' +
                        '                            <div class="col-md-8">' +
                        '                        <h7 class="card-title">'+ archivos[i].name +' </h7>' +
                        '                            </div>' +

                        '                            <div class="col-md-3">' +

                        '                   <button type="button" id="quitar" data-id="'+ index +'" class="btn btn-warning rounded-round btn-sm"><i class="icon-cross3"></i> </button>' +

                        '                            </div>' +
                        '                        </div>' +
                        '                    </div>' +
                        '                </div>' +
                        '            </div>'

                }
            }
            $('#doc-card').append(card);
            console.log(files_array);

        });

        $('#doc-card').on('click','.card #quitar',function(){
            var id = $(this).data('id');
            $(this).parent().parent().parent().parent().parent().hide();
            /**COMPROBAR SI HAY EMPTYS***/
            delete files_array[id];
            console.log(files_array);
        });

        $('#subir').click(function(){
            var idocurrencia = $('#ocurrencia').val();
            if(files_array.length > 0 ){
                for (var i = 0; i < files_array.length; i++) {
                    if(files_array[i]){
                        var files = new FormData();
                        files.append('idocurrencia',idocurrencia);
                        files.append('archivo',files_array[i]);
                        jQuery.ajax({
                            url: '{{ route('quploadfile_ocurrencia') }}',
                            data: files,
                            cache: false,
                            contentType: false,
                            processData: false,
                            method: 'POST',
                            type: 'POST', // For jQuery < 1.9
                            success: function(response){
                                swal({
                                    title: 'Listo!',
                                    text: 'Se subieron los documentos',
                                    icon: 'success'
                                }).then(function() {
                                    $('#btnEnviar').attr('disabled',false);
                                });
                            }
                        });

                    }
                }
            }else{
                swal({
                    title: 'Upps!',
                    text: 'Agrega un nuevo Documento!',
                    icon: 'warning'
                })
            }
            files_array = [];
        });
        var max = 150

        $('#descripcion').keypress(function(e){
            if (e.which < 0x20) {
                // e.which < 0x20, then it's not a printable character
                // e.which === 0 - Not a character
                return;     // Do nothing
            }
            if (this.value.length == max) {
                e.preventDefault();
            } else if (this.value.length > max) {
                // Maximum exceeded
                this.value = this.value.substring(0, max);
            }
        }).keyup(function(){
            var cuenta = $(this).val();
            var count = $('#falta_count').html();
            var residuo = 150 - parseInt(cuenta.length);
            $('#falta_count').html(residuo);
        });


        $('#btnEnviar').click(function(){
            var idocurrencia = $('#ocurrencia').val();

            swal({
                title: "Enviar",
                text: "Seguro que deseas enviar la ocurrencia?",
                icon: "info",
                buttons: [
                    'No, cancelar!',
                    'Si'
                ],
                dangerMode: true,
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.post('{{ route('qenviar_ocurrencias') }}',{'idocurrencia':idocurrencia},function(data){
                        swal({
                            title: 'Listo!',
                            text: 'Se envio la ocurrencia',
                            icon: 'success',
                            dangerMode: true,
                        }).then(function() {
                            window.location.href = "{{ route('listas') }}";
                        });
                    });
                }
            });
        });

    </script>
@endsection
