@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="assets/css/styles.css">
    <meta name="csrf-token" content="{{ Session::token() }}">



    <div class="admin-content">

        <div class="content-oh">

            <form class="sanciones">
                <div class="conteiner-fluid">
                    <div class="row">

                        <div class="row" style="padding: 10px; background-color: #dcdcdd">
                            <div class="col-md-12" style="background-color: #fff; padding-top: 20px">
                                <div class="form-group row" id="div-fecha">
                                    <label for="reincidencias-input" class="col-sm-2 col-form-label">Fecha de la Incidencia</label>
                                    <div class="col-sm-8">
                                        <input class="datepicker" style="width: 100% ;" id="fecha" data-date-format="yyyy-mm-dd" value="{{$data['ocurrencia']->fec_ocurrencia}}">
                                    </div>
                                </div>
                                <div class="form-group row" id="div-afiliado">
                                    <label for="sansion-input" class="col-sm-2 col-form-label">Afiliados</label>
                                    <div class="col-sm-8">
                                        <select class="custom-select" id="afiliados" style="width: 100% !important; background-position-x: 700px !important">
                                            <option selected disabled>{{$data['ocurrencia']->nombres}}</option>
                                         </select>
                                    </div>
                                    <input type="hidden" id="idocurrencia" value="{{$data['ocurrencia']->idocurrencia}}">
                                </div>

                                <div class="form-group row" id="div-afiliado">
                                    <label for="empresa-select" class="col-sm-2 col-form-label">Faltas</label>
                                    <div class="col-sm-8">
                                        <select class="custom-select" id="faltas" style="width: 100% !important; background-position-x: 700px !important">
                                            <option value="{{$data['ocurrencia']->idfalta}}">{{$data['ocurrencia']->falta}}</option>
                                            @forelse( $data['faltas'] as $index => $value)
                                                @if($data['ocurrencia']->idfalta !== $value->idfalta  )
                                                <option value="{{$value->idfalta}}"> {{ $value->falta }}</option>
                                                @endif
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row  has-error has-feedback" id="div-afiliado">
                                    <label for="empresa-select" class="col-sm-2 col-form-label">Sancion</label>
                                    <div class="col-sm-8">
                                        <select class="custom-select" id="sancion" style="width: 100% !important; background-position-x: 700px !important">
                                            <option value="{{$data['ocurrencia']->idsancion}}">{{$data['ocurrencia']->sancion}}</option>
                                            @forelse( $data['sanciones'] as $index => $value)
                                                @if($data['ocurrencia']->idsancion !==$value->idsancion )
                                                <option value="{{$value->idsancion}}"> {{ $value->sancion }}</option>
                                                @endif
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row" id="div-ini">
                                    <label for="reincidencias-input" class="col-sm-2 col-form-label">Fecha de inicio de la Suspencion</label>
                                    <div class="col-sm-8">
                                        <input class="datepicker" id="inicio" data-date-format="yyyy-mm-dd" >
                                    </div>
                                </div>
                                <div class="form-group row" id="div-fin">
                                    <label for="reincidencias-input" class="col-sm-2 col-form-label">Fecha final de la Suspencion</label>
                                    <div class="col-sm-8">
                                        <input class="datepicker" id="fin" data-date-format="yyyy-mm-dd">
                                    </div>
                                </div>

                                <div class="form-group row" id="div-descripcion">
                                    <label for="reincidencias-input" class="col-sm-2 col-form-label">Descripcion</label>
                                    <div class="col-sm-8">
                                        <textarea type="text" class="form-control" id="descripcion" placeholder="Todos">{{$data['ocurrencia']->descripcion}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row " id="div-calificacion">
                                    <label for="empresa-select" class="col-sm-2 col-form-label">Calificaciones</label>
                                    <div class="col-sm-3">
                                        @forelse( $data['calificaciones'] as $index => $value)
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="calificaciones" name="calificaciones" style="  height: 12px; " value="{{$value->idcalificacion}}">
                                                    {{ $value->calificacion }}
                                                </label>
                                            </div>
                                        @empty
                                        @endforelse
                                    </div>
                                </div>
                                <!--<div class="form-group row">
                                    <label for="reincidencias-input" class="col-sm-2 col-form-label">Archivos:</label>
                                    <div class="col-sm-10">
                                        <div class="file-loading">
                                            <input id="archivos" class="file" type="file" multiple data-min-file-count="3">
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                            <div class="row">
                                <div class="pull-right" style="padding-top: 10px; padding-right: 10px">
                                    <button type="button" id="guardar" class="btn btn-primary"><img src="assets/img/nuevo.svg"> Guardar</button>
                                    @if($data['ocurrencia']->state == 2)
                                        <div>
                                            <input type="radio" value="0"> Aceptar
                                        </div>
                                        <div>
                                            <input type="radio" value="1"> Denegar
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </div>


                        </div>
                    </div>

            </form>



    </div>


    <script src="assets/js/scripts.js?v=0.1"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <script type="application/javascript">
        $('#fecha').datepicker();
        $('#inicio').datepicker();
        $('#fin').datepicker();

        $('#div-ini').hide();
        $('#div-fin').hide();

        $('#sancion').change(function(){
            var id = $(this).val();
            console.log(id);
            if(id === '1'){
                $('#div-ini').show();
                $('#div-fin').show();
            }else{
                $('#div-ini').hide();
                $('#div-fin').hide();
            }
        });


        var calificaciones = "{{$data['ocurrencia']->calificacion }}";
        var obj = JSON.parse(calificaciones.replace(/&quot;/g,'"'));

        for(i = 0; i < obj.length ; i++){
            $(":checkbox[value="+obj[i]+"]").attr("checked","true");
        }


        $('#guardar').click(function(){
            if(!validacion()){
                swal({
                    title: "Campos incompletos!",
                    text: "Falta completar campos",
                    icon: "warning",
                });
                return;
            }
            var calificaciones = [];
            $("input:checkbox[name=calificaciones]:checked").each(function(){
                calificaciones.push($(this).val());
            });

            var calificacion = JSON.stringify(calificaciones);
            var fecha = $('#fecha').val();
            var inicio =$('#inicio').val();
            var fin =$('#fin').val();
            var afiliado = $('#afiliados').val();
            var falta = $('#faltas').val();
            var sancion = $('#sancion').val();
            var descripcion = $('#descripcion').val();
            var archivos = $('#archivos').prop("files");
            var idocurrencia = $('#idocurrencia').val();




            var array = {'_token': $('meta[name=csrf-token]').attr('content'),idocurrencia:idocurrencia, calificacion:calificacion, fecha:fecha, inicio:inicio, fin:fin, afiliado:afiliado,falta:falta,sancion:sancion,descripcion:descripcion};

            $.post('{{ route('qmodificar_ocurrencia') }}',array,function(data){
                limpiar();
                var response = JSON.parse(data);
                /*for (var i = 0; i < archivos.length; i++) {
                    var data = new FormData();
                    data.append('idocurrencia',response.data.idocurrencia);
                    data.append('archivo',archivos[i]);

                    jQuery.ajax({
                        url: '{{ route('quploadfile_ocurrencia') }}',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        success: function(data){
                            console.log(data);
                        }
                    });

                }

                   */
                    swal({
                        title: "Se envio el formulario.",
                        text: response.mensaje,
                        icon: "success"
                    });
                window.location.href = "{{ route('listas') }}";
            });
        });

        function validacion(){
            var val = true;
            var fecha = $('#fecha').val();
            var afiliado = $('#afiliados').val();
            var falta = $('#faltas').val();
            var sancion = $('#sancion').val();

            if(fecha === ""){
                $('#div-fecha').addClass('has-error');
                val = false;
            }
            if(afiliado === ""){
                $('#div-afiliado').addClass('has-error');
                val = false;

            }
            if(falta === ""){
                $('#div-falta').addClass('has-error');
                val = false;

            }
            if(sancion === ""){
                $('#div-sancion').addClass('has-error');
                val = false;

            }
            return val;


        }

        function limpiar(){
            var fecha = $('#fecha').val('');
            var inicio =$('#inicio').val('');
            var fin =$('#fin').val('');
            var afiliado = $('#afiliados').val('');
            var falta = $('#faltas').val('');
            var sancion = $('#sancion').val('');
            var descripcion = $('#descripcion').val('');
            var archivos = $('#archivos').prop("files");
        }
    </script>
@endsection
