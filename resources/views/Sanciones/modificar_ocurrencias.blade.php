@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="assets/css/styles.css">

    <meta name="csrf-token" content="{{ Session::token() }}">

    <div class="row">
        <div class="col-md-6 ">
            <button class="btn btn-success" id="subir" disabled>
                <i class="icon-floppy-disk"></i>
                Subir y Enviar
            </button>
            <button class="btn btn-success" id="btnmodificar" >
                <i class="icon-floppy-disk"></i>
                    Modificar
            </button>
            <a href="{{ route('listas') }}" class="btn btn-danger" id="btnsalir" >
                <i class="icon-arrow-up-left2"></i> Salir
            </a>
        </div>
    </div>
    <div class="row div-row">
        <div class="col-md-12">
            <div class="form-group">
                @forelse($logs as $log)
                    @if($log->estado == 1)
                        <div class="alert alert-info alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0">
                            {{ $log->detalle }}<br>
                            <small>{{$log->fecha_creacion}}</small>

                        </div>
                    @else
                    @endif
                @empty
                    <div class="alert alert-info alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0">
                        <span class="font-weight-semibold">Upps!</span> Documentacion insuficiente, debes agrenar nuevas.!
                    </div>
                @endforelse
            </div>
        </div>
    </div>

    <div class="row div-row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Colaborador</label>
                </div>
                <div class="col-md-10">
                        <input type="text" id="colaborador" value="{{ $ocurrencia->nombres }}" placeholder="Colaborador" disabled>
                    <div class="list-colaboradores">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Falta</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="falta" value="{{ $ocurrencia->falta }}" placeholder="falta" disabled>
                    <div class="list-items">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Sanción</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="sancion" value="{{ $ocurrencia->sancion }}"  disabled>
                    <span class="form-text text-muted text-teal text-right">Selecciona una Falta </span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Reincidencia</label>
                </div>
                <div class="col-md-10">
                    <input type="text" value="{{ $ocurrencia->reincidencia }}" id="reincidencia" placeholder="reincidencia" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Motivo de la Sanción</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group row">

                    <textarea type="text" id="descripcion"   style="width:100%;" disabled>{{ $ocurrencia->descripcion }}</textarea>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Estado</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="estado" value="Borrador" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> F. Ocurrencia</label>
                </div>
                <div class="col-md-10">
                    <input type="text" data-date-format="yyyy-mm-dd" value="{{ $ocurrencia->created_at }}" id="fec_ocurrencia" placeholder="Y-m-d" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Fecha suspensión </label>
                </div>
                <div class="col-md-5">
                    <input type="text" data-date-format="yyyy-mm-dd" id="fec_ini" value="{{ $ocurrencia->fec_suspencion_ini }}" placeholder="Inicio de suspensión  Y-m-d" disabled>
                </div>
                <div class="col-md-5">
                    <input type="text" data-date-format="yyyy-mm-dd"  id="fec_fin" value="{{ $ocurrencia->fec_suspencion_fin }}" placeholder="Fin de la suspensión  Y-m-d" disabled>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12" >
                    <div class="form-group row">
                                   <label for="archivos" class="col-sm-2 col-form-label">Cargar documentos:</label>
                                   <div class="col-sm-10">
                                       <div class="file-loading border-teal">
                                           <input id="archivos" class="file" type="file" multiple>
                                           <span class="form-text text-muted text-danger text-right">Los archivos no pueden pesar mas de 5MB </span>
                                            
                                           <input type="hidden" id="ocurrencia" class="border-teal" value="{{ $ocurrencia->idocurrencia }}">
                                       </div>
                                       <div class="mensaje"></div>
                                   </div>
                               </div>
                </div>
            </div>
        </div>
    </div>

        <script type="application/javascript">
            var ocurrencia = "{{ $ocurrencia->idocurrencia }}";
            var colaborador = "{{ $ocurrencia->idafiliado }}";
            var sancion = "{{ $ocurrencia->idsancion }}";
            var rol = "{{ json_encode(Auth::user()->idrol ) }}";

            $('#fec_ocurrencia').datepicker({
                startDate: '-1m',
                endDate: '+1d',
                datesDisabled: '+1d',
            });
            $('#subir').attr('disabled',false);
            $('#reincidencia').attr('disabled',true);
            $('#falta').attr('disabled',true);
            $('#descripcion').attr('disabled',true);
            $('#fec_ocurrencia').attr('disabled',true);
            if(sancion === "1"){
                $('#fec_ini').attr('disabled',false);
                $('#fec_fin').attr('disabled',false);
            }

            $('#falta').attr('disabled',true);
            $('#colaborador').attr('disabled',true);
            $('#btnmodificar').hide();

            $('#subir').click(function(){
                var idocurrencia = ocurrencia;
                var archivos = $('#archivos').prop("files");
                var idcolaborador = colaborador;
                var arr = {idocurrencia:idocurrencia,idcolaborador:idcolaborador};
                if(archivos.length < 1){
                    swal("Upps","Debes agregar al menos un documento","error");
                    return false;
                }
                $.post('{{ route('qenviarmodifiacion') }}',arr,function(data){
                    for (var i = 0; i < archivos.length; i++) {
                        var FileSize = archivos[i].size / 1024 / 1024; // in MB
                        if (FileSize > 5) {
                            swal({
                            title: 'Upps!',
                            text: "El archivo "+ archivos[i].name +" es muy grande" ,
                            icon: 'warning'
                        })
                        } else {
                            var files = new FormData();
                            files.append('idocurrencia',idocurrencia);
                            files.append('archivo',archivos[i]);
                            jQuery.ajax({
                                url: '{{ route('quploadfile_ocurrencia') }}',
                                data: files,
                                cache: false,
                                contentType: false,
                                processData: false,
                                method: 'POST',
                                type: 'POST', // For jQuery < 1.9
                                success: function(response){
                                    swal({
                                        title: 'Listo!',
                                        text: 'Se subieron los documentos',
                                        icon: 'success'
                                    });
                                  window.location.href = "{{ route('listas') }}";

                                }
                            });
                        }
                    }

                });


            });

            $('#btnmodificar').click(function(){
                var fec_ini = $('#fec_ini').val();
                var fec_fin = $('#fec_fin').val();
                var idocurrencia = ocurrencia;
                var arr = {idocurrencia:idocurrencia,fec_suspencion_ini:fec_ini, fec_suspencion_fin:fec_fin};
                $.post('{{ route('qmodificar_ocurrencia') }}',arr,function(data){
                    var response = JSON.parse(data);
                    if(response.status == 200){
                        swal({
                            title: 'Listo!',
                            text: 'Se modifico la ocurrencia #'+ocurrencia,
                            icon: 'success'
                        });
                        window.location.href = "{{ route('listas') }}";

                    }else{
                        swal({
                            title: 'Upps!',
                            text: 'Intentalo nuevamente.',
                            icon: 'warning'
                        });
                    }
                });


            })
        </script>
@endsection
