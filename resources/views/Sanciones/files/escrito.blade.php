<style>
    @font-face {
        font-family: 'avangart';
        src: url('{{ asset("/fonts/Roboto-Light.ttf") }}')format('truetype');
        font-style: normal;
        font-weight: lighter;
    }
    body{
        font-family: avangart, Arial, Helvetica, sans-serif;
        font-size: 10pt;
    }
    #contenido{
        margin: 0.3cm 0.3cm;
    }
    .cabecera{
        text-align: left;
        padding: 10px 0;
    }
    .linea{
        text-transform: capitalize;
        padding: 0  0.1cm;
    }
    .titulo{
        text-transform: uppercase;
        text-align: center;
        text-decoration: underline;
        font-size: 11pt;
        padding:0 0.3cm;
    }
    .final{
        text-transform: uppercase;
        text-align: center;
        padding-top: 1cm;
    }
    #nombre{
        text-transform: uppercase;
    }
    #presente{
        text-decoration: underline;
    }
    .contenido{
        text-align: justify;
    }
</style>
<div id="contenido">
    <div class="cabecera">
        <div class="linea"><?php echo "CMP-FRM001" ;?></div>
        <br>
        <div class="linea"><?php echo "Versión 01-666" ;?></div>
    </div>
    <div class="titulo">
        CARTA DE AMONESTACIÓN
    </div>

    <div class="cabecera">
        Colaborador:
        <br>
        <div id="nombre"> {{ $ocurrencia->nombres }}</div>
        <div id="nombre">DNI: {{ $ocurrencia->idafiliado }}</div>


        <br>
        <div id="presente"> Presente.-</div>
    </div>
    <div class="contenido">
        <p>De nuestra mayor consideración. Mediante la presente comunicación, y al amparo de lo previsto por el artículo 9 del Decreto Supremo No 003-97- TR, Ley de Productividad y Competitividad Laboral, el cual establece que todo empleador para el cumplimiento de las obligaciones laborales, tiene las facultades de dictar las órdenes necesarias para la ejecución de las mismas, y sancionar disciplinariamente, dentro de los límites de la razonabilidad, cualquier infracción o incumplimiento de las obligaciones a cargo del trabajador, debemos informarle lo siguiente:</p>
        <p>Usted ha incurrido en faltas al cumplimiento de sus obligaciones laborales en el siguiente sentido: </p>
        <p>
            Falta: {{ $ocurrencia->falta }} .</p><p>
            Fecha: {{ $ocurrencia->fec_ocurrencia }}
        </p>
        <p>Gravedad: <?php
            switch ( $ocurrencia->gravedad )    {
                case 0:
                    echo 'Leve';
                    break;
                case 1:
                    echo "Moderada";
                    break;
                case 2:
                    echo "Grave";
                    break;
            }
            ?></p>
        <p>Reincidencia: {{ $ocurrencia->reincidencia }}</p>
        <p>
            {{ $ocurrencia->descripcion }}.
        </p>
        <p><b>Reglamento:</b></p>
        @forelse($ocurrencia->legal as $legal)
            @if($legal[0]->anombre)
                <p> {{ $legal[0]->anombre }}</p>
            @endif
            <p> {{ $legal[0]->pnombre }}</p>
            <p>{{ $legal[0]->titulo }}:
                "{{ $legal[0]->descripcion }}"
            </p>
        @empty
        @endforelse
        <p>Esta actitud refleja una falta al cumplimiento de sus obligaciones y el compromiso que tiene para con la empresa, toda vez que previamente ha sido amonestado de manera verbal invocándolo al cumplimiento adecuado de sus obligaciones laborales, este hecho quiebra la confianza depositada en usted, habiendo incumplido lo establecido en el Reglamento Interno de Trabajo, en el sentido de lo siguiente: Igualmente este hecho significa un desinterés y falta de dedicación en el cumplimiento de sus labores.
        </p>
        <p>Sanción: {{$ocurrencia->sancion}}</p>
        @if($ocurrencia->fec_suspencion_ini)
            <p>Fecha de inicio de Suspensión:</p>
            <p>{{ $ocurrencia->fec_suspencion_ini }}</p>
            <p>Fecha de fin de Suspensión:</p>
            <p>{{ $ocurrencia->fec_suspencion_fin }}</p>
        @endif

        <p>
            Deseamos que esta sanción sirva para la reflexión y se evite reiteración, dado que de continuar con esta actitud nos veremos obligados a tomar las acciones legales pertinentes conforme el ordenamiento laboral lo establece. Debemos indicarle que si en un plazo no mayor a horas de recepcionado el presente documento, no se ha manifestado al respecto, se entenderá como ratificado el mismo.
        </p>
        <p>Por último, le solicitamos cumpla con sus obligaciones laborales y corrija su conducta a fin de poder sujetar la misma a las directivas de nuestra organización. </p>
        <p>
            Atentamente. </p>
        <p>



    </div>
    <div class="final">   <p>{{ $colaborador->nombres }} </p><p>
            {{ $colaborador->puesto }} </p></div>


</div>

