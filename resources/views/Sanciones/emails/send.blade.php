<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>

<head>
    <meta charset="utf-8">

    <style>
        .ExternalClass {
            width: 100%;
        }
        /* Forces Outlook.com to display emails at full width */

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }
        /* Forces Outlook.com to display normal line spacing, here is more on that: http://www.emailonacid.com/forum/viewthread/43/ */

        .imageFix {
            display: block;
        }

        a {
            text-decoration: none;
        }

        @media screen and (max-width: 600px) {
            table.row th.col-lg-1,
            table.row th.col-lg-2,
            table.row th.col-lg-3,
            table.row th.col-lg-4,
            table.row th.col-lg-5,
            table.row th.col-lg-6,
            table.row th.col-lg-7,
            table.row th.col-lg-8,
            table.row th.col-lg-9,
            table.row th.col-lg-10,
            table.row th.col-lg-11,
            table.row th.col-lg-12 {
                display: block;
                width: 100% !important;
            }
            .d-mobile {
                display: block !important;
            }
            .d-desktop {
                display: none !important;
            }
        }

        @media yahoo {
            .d-mobile {
                display: none !important;
            }
            .d-desktop {
                display: block !important;
            }
        }
    </style>
</head>

<body style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; border: 0; box-sizing: border-box; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; height: 100%; line-height: 24px; margin: 0; min-width: 100%; outline: 0; padding: 0; width: 100%;">
<table class="container" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <tr>
        <td align="center" style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0; padding: 0px 16px 0 16px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center">
                <tbody>
                <tr>
                    <td width="600">
            <![endif]-->
            <table class="hr" style="border: 0; border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0; padding: 16px 0px;" width="100%">
                        <table style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr style="border-bottom: 2px solid #dddddd;">
                                <td style="border-collapse: collapse;text-align: right; border-spacing: 0px;  font-size: 16px; line-height: 24px; margin: 0;" width="100%" height="1px">
                                    <img width="200" src="https://app.mapsalud.com/clientes/somosoh/sanciones/public/assets/img/email/logo.png">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table align="center" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0;text-align: left;">

                        <h1 class="text-center" style="color: inherit;text-align: left !important; font-size: 24px; font-weight: 500; line-height: 39.6px; margin-bottom: 8px; margin-top: 0; text-align: center; vertical-align: baseline; color: #b3272d">
                            Sistema de Sanciones [ ocurrencia N° {{ $ocurrencia->idocurrencia  }} ]
                        </h1>
                        <span class="text-center text-muted" style="color: #636c72; font-size: 20px; font-weight: 500; line-height: 26.4px; margin-bottom: 8px; margin-top: 0; text-align: center; vertical-align: baseline;">
                  	ID OCURRENCIA:
                  </span>
                        <span class="text-center text-muted" style="color: #636c73; font-size: 20px; font-weight: bolder; line-height: 26.4px; margin-bottom: 8px; margin-top: 0; text-align: center; vertical-align: baseline;">
                  	 {{ $ocurrencia->idocurrencia  }}
                  </span>
                        <br>
                        <span class="text-center text-muted" style="color: #636c72; font-size: 20px; font-weight: 500; line-height: 26.4px; margin-bottom: 8px; margin-top: 0; text-align: center; vertical-align: baseline;">
                  	ESTADO:
                  </span>
                        <span class="text-center text-muted" style="color: #636c72; font-size: 20px; font-weight: bolder; line-height: 26.4px; margin-bottom: 8px; margin-top: 0; text-align: center; vertical-align: baseline;">
                  	    <?php
                            switch ($ocurrencia->estado){
                                case 0:
                                    echo "Enviado Aprobador";
                                    break;
                                case 1:
                                    echo "Rechazado modificación";
                                    break;
                                case 2:
                                    echo "Ocurrencia Modificada";
                                    break;
                                case 3:
                                    echo "Aceptado Gerencia";
                                    break;
                                case 4:
                                    echo "Denegado Gerencia";
                                    break;
                                case 5:
                                    echo "Aprobado Relab";
                                    break;
                                case 6:
                                    echo "Denegado Relab";
                                    break;
                                case 7:
                                    echo "Cerrado";
                                    break;
                                case -1:
                                    echo "Creado";
                                    break;
                            }
                            ?>  /  {{ $estado }}
                        </span>
                        <br>
                        <span class="text-center text-muted" style="color: #636c72; font-size: 20px; font-weight: 500; line-height: 26.4px; margin-bottom: 8px; margin-top: 0; text-align: center; vertical-align: baseline;">
                  	FECHA DE FALTA:
                  </span>
                        <span class="text-center text-muted" style="color: #636c72; font-size: 20px; font-weight: bolder; line-height: 26.4px; margin-bottom: 8px; margin-top: 0; text-align: center; vertical-align: baseline;">
                  	{{ $ocurrencia->fec_ocurrencia }}
                  </span>
                        <br><br>


                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </tbody>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>


<table class="container bg-light p-3" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" bgcolor="#f8f9fa" width="100%">
    <tbody>
    <th>
        <table align="center" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
                <td style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0;text-align: left;">

                    <h1 class="text-center" style="color: inherit;text-align: left !important; font-size: 18px; font-weight: 500; line-height: 39.6px; margin-bottom: 8px; margin-top: 0; text-align: center; vertical-align: baseline; color: #b3272d">
                        COLABORADOR:
                    </h1>
                </td>
            </tr>
            </tbody>
        </table>
    </th>
    <tr>
        <td align="center" style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0; padding: 16px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center">
                <tbody>
                <tr>
                    <td width="600">
            <![endif]-->
            <table align="center" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0;">

                        <table class="row" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; margin-left: -15px; margin-right: -15px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; table-layout: fixed;" border="0" cellpadding="0"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>

                                <table class="table table-striped thead-default table-bordered" style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; margin-bottom: 16px; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
                                    <tbody>
                                    <tr style="" bgcolor="#f2f2f2">
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Documento de Identidad</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;font-weight: bolder;color: #636c72;" valign="top">{{ $ocurrencia->idafiliado }}</td>
                                    </tr>
                                    <tr>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Nombres y Apellidos</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;font-weight: bolder;color: #636c72;" valign="top">{{ $ocurrencia->nombres  }}</td>
                                    </tr>
                                    <tr class="table-success" style="">
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;"  valign="top">Puesto</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;font-weight: bolder;color: #636c72;"  valign="top">{{ $ocurrencia->puesto }}</td>
                                    </tr>
                                    <tr>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Área</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;font-weight: bolder;color: #636c72;" valign="top">{{ $ocurrencia->area  }}</td>
                                    </tr>
                                    <tr>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Fecha de Ingreso</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;font-weight: bolder;color: #636c72;" valign="top">{{ $ocurrencia->fec_contratacion  }}</td>
                                    </tr>
                                    <tr>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Sección</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;font-weight: bolder;color: #636c72;" valign="top">{{ $ocurrencia->seccion  }}</td>
                                    </tr>
                                    </tbody>
                                </table>

                            </tr>



                            </thead>
                        </table>


                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </tbody>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>

<!-- FALTA -->
<table class="container bg-light p-3" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" bgcolor="#f8f9fa" width="100%">
    <tbody>
    <tr>
        <td align="center" style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0; padding: 16px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center">
                <tbody>
                <tr>
                    <td width="600">
            <![endif]-->
            <table align="center" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0;">

                        <table class="row" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; margin-left: -15px; margin-right: -15px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; table-layout: fixed;" border="0" cellpadding="0"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>

                                <table class="table table-striped thead-default table-bordered" style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; margin-bottom: 16px; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
                                    <tbody>
                                    <tr style="" bgcolor="#f7e4e5">
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Falta:</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;font-weight: bolder;color: #636c72; text-transform: uppercase;" valign="top">{{ $ocurrencia->falta  }}</td>
                                    </tr>
                                    <tr>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Gravedad:</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;color: #636c72;text-transform: uppercase;" valign="top">
                                            <?php
                                                switch ( $ocurrencia->gravedad )    {
                                                    case 0:
                                                        echo 'Leve';
                                                        break;
                                                    case 1:
                                                        echo "Moderada";
                                                        break;
                                                    case 2:
                                                        echo "Grave";
                                                        break;
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Motivo de Sanción:</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;color: #636c72;" valign="top">{{ $ocurrencia->descripcion  }}</td>
                                    </tr>
                                    <tr class="table-success" style="">
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;"  valign="top">Reincidencia: </td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;color: #636c72;text-transform: uppercase;"  valign="top">{{ $ocurrencia->reincidencia  }}</td>
                                    </tr>
                                    <tr bgcolor="#cbf0ff">
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top" colspan="2">Reglamento:</td>
                                    </tr>

                                    @forelse($ocurrencia->legal as $legal)
                                        @if($legal[0]->anombre)
                                        <tr>
                                            <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;font-weight: bolder;color: #636c72;text-transform: uppercase;" valign="top" colspan="2">
                                                {{ $legal[0]->anombre }}
                                            </td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td colspan="2" style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;color: #636c72;" valign="top">
                                                {{ $legal[0]->pnombre }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">{{ $legal[0]->titulo }}</td>
                                            <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;color: #636c72;" valign="top">
                                                {{ $legal[0]->descripcion }}
                                            </td>
                                        </tr>
                                        @empty
                                        @endforelse


                                    </tbody>
                                </table>
                            </tr>



                            </thead>
                        </table>


                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </tbody>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>

<!-- SANCION -->
<table class="container bg-light p-3" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" bgcolor="#f8f9fa" width="100%">
    <tbody>
    <tr>
        <td align="center" style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0; padding: 16px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center">
                <tbody>
                <tr>
                    <td width="600">
            <![endif]-->
            <table align="center" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0;">

                        <table class="row" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; margin-left: -15px; margin-right: -15px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; table-layout: fixed;" border="0" cellpadding="0"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>

                                <table class="table table-striped thead-default table-bordered" style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; margin-bottom: 16px; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
                                    <tbody>
                                    <tr style="" bgcolor="#f7e4e5">
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Sanción:</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;font-weight: bolder;color: #636c72; text-transform: uppercase;" valign="top">{{ $ocurrencia->sancion }}</td>
                                    </tr>
                                    @if($ocurrencia->estado == 7 and $ocurrencia->idsancion != 2 )
                                        <tr>
                                            <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Formato</td>
                                            <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;color: #636c72;text-transform: uppercase;" valign="top">
                                                <a href="https://app.mapsalud.com/clientes/somosoh/sanciones/public/get_formato?idocurrencia={{ $ocurrencia->idocurrencia }}">
                                                    <img width="200" src="https://app.mapsalud.com/clientes/somosoh/sanciones/public/images/formato.png" alt="">

                                                </a>
                                            </td>
                                        </tr>
                                    @endif
                                    @if($ocurrencia->fec_suspencion_ini)
                                    <tr>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">Fecha de inicio de Suspensión:</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;color: #636c72;text-transform: uppercase;" valign="top">{{ $ocurrencia->fec_suspencion_ini }}</td>
                                    </tr>
                                    <tr class="table-success" style="">
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;"  valign="top">Fecha de fin de Suspensión:</td>
                                        <td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;color: #636c72;text-transform: uppercase;"  valign="top">{{ $ocurrencia->fec_suspencion_fin }}</td>
                                    </tr>
                                    @endif

                                    </tbody>
                                </table>

                            </tr>



                            </thead>
                        </table>


                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </tbody>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>

{{--<!-- RESPONSABLES -->--}}
{{--<table class="container bg-light p-3" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" bgcolor="#f8f9fa" width="100%">--}}
    {{--<tbody>--}}
    {{--<th>--}}
        {{--<table align="center" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">--}}
            {{--<tbody>--}}
            {{--<tr>--}}
                {{--<td style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0;text-align: left;">--}}

                    {{--<h1 class="text-center" style="color: inherit;text-align: left !important; font-size: 18px; font-weight: 500; line-height: 39.6px; margin-bottom: 8px; margin-top: 0; text-align: center; vertical-align: baseline; color: #b3272d">--}}
                        {{--RESPONSABLES:--}}
                    {{--</h1>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--</tbody>--}}
        {{--</table>--}}
    {{--</th>--}}
    {{--<tr>--}}
        {{--<td align="center" style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0; padding: 16px;">--}}
            {{--<!--[if (gte mso 9)|(IE)]>--}}
            {{--<table align="center">--}}
                {{--<tbody>--}}
                {{--<tr>--}}
                    {{--<td width="600">--}}
            {{--<![endif]-->--}}
            {{--<table align="center" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">--}}
                {{--<tbody>--}}
                {{--<tr>--}}
                    {{--<td style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0;">--}}

                        {{--<table class="row" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; margin-left: -15px; margin-right: -15px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; table-layout: fixed;" border="0" cellpadding="0"--}}
                               {{--cellspacing="0" width="100%">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}

                                {{--<table class="table table-striped thead-default table-bordered" style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; margin-bottom: 16px; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">--}}
                                    {{--<tbody>--}}
                                    {{--<tr>--}}
                                        {{--<td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">REGISTRADOR:</td>--}}
                                        {{--<td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;color: #636c72;" valign="top">--}}
                                            {{--Juan Cruz <br>--}}
                                            {{--DNI: 20154563 <br>--}}
                                            {{--Supervisor Finantiendas Jockey <br>--}}
                                            {{--email: 	<span style="color: #00a1e4"><a href="mailto:juan.cruz@somosoh.pe"></a> juan.cruz@somosoh.pe</span></td>--}}
                                    {{--</tr>--}}
                                    {{--<tr>--}}
                                        {{--<td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;" valign="top">APROBADOR:</td>--}}
                                        {{--<td style="border: 1px solid #e9ecef; border-collapse: collapse; border-spacing: 0px; border-top: 1px solid #e9ecef; font-size: 16px; line-height: 24px; margin: 0; padding: 12px;color: #636c72;" valign="top">--}}
                                            {{--Juan Cruz <br>--}}
                                            {{--DNI: 20154563 <br>--}}
                                            {{--Supervisor Finantiendas Jockey <br>--}}
                                            {{--email: 	<span style="color: #00a1e4"><a href="mailto:juan.cruz@somosoh.pe"></a> juan.cruz@somosoh.pe</span></td>--}}
                                    {{--</tr>--}}

                                    {{--</tbody>--}}
                                {{--</table>--}}

                            {{--</tr>--}}



                            {{--</thead>--}}
                        {{--</table>--}}


                    {{--</td>--}}
                {{--</tr>--}}
                {{--</tbody>--}}
            {{--</table>--}}
            {{--<!--[if (gte mso 9)|(IE)]>--}}
            {{--</td>--}}
            {{--</tr>--}}
            {{--</tbody>--}}
            {{--</table>--}}
            {{--<![endif]-->--}}
        {{--</td>--}}
    {{--</tr>--}}
    {{--</tbody>--}}
{{--</table>--}}
<table class="container bg-dark" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" bgcolor="#343a40" width="100%">
    <tbody>
    <tr>
        <td align="center" style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0; padding: 0px 16px 0 16px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center">
                <tbody>
                <tr>
                    <td width="600">
            <![endif]-->
            <table align="center" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0;">

                        <div class="m-4" style="margin: 24px;">
                            <div class="text-light text-center m-4 w-100" style="color: #f8f9fa; margin: 24px; text-align: center; width: 100%;">
                                <div class="links-content">
                                    <div style="margin-top:10px; margin-bottom: 20px;">
                                        <span class="descarga-el-app">Verfica en web y App para Android y Iphone.</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm playstore">
                                            <a href="https://app.mapsalud.com/clientes/somosoh/sanciones/public/">
                                                <h1 class="text-center" style="color: inherit;text-align: left !important; font-size: 18px; font-weight: 500; line-height: 39.6px; margin-bottom: 8px; margin-top: 0; text-align: center; vertical-align: baseline; color: #b3272d">
                                                    Versión Web
                                                </h1>
                                            </a>
                                        </div>
                                        <div class="col-sm playstore">
                                            <a href="https://play.google.com/store/apps/details?id=com.mapsalud.wl.appsomosoh">
                                            Android</a>
                                        </div>
                                        <div class="col-sm appstore">
                                            <a href="https://itunes.apple.com/pe/app/somos-oh/id1262543814?mt=8">
                                            IOS</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </tbody>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>

<table class="container bg-dark" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" bgcolor="#343a40" width="100%">
    <tbody>
    <tr>
        <td align="center" style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0; padding: 0px 16px 0 16px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center">
                <tbody>
                <tr>
                    <td width="600">
            <![endif]-->
            <table align="center" style="border-collapse: collapse; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td style="border-collapse: collapse; border-spacing: 0px; font-size: 16px; line-height: 24px; margin: 0;">

                        <div class="m-4" style="margin: 24px;">
                            <div class="text-light text-center m-4 w-100" style="color: #f8f9fa; margin: 24px; text-align: center; width: 100%;">
                                &#xA9;2018 Sistema de Sanciones Somosoh
                            </div>
                        </div>


                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </tbody>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>

</body>

</html>