@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="assets/css/styles.css">
    <meta name="csrf-token" content="{{ Session::token() }}">
<?PHP $afiliado = Auth::user(); ?>
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-info" id="rastrear">
                <i class="icon-git-branch"></i>
                Rastrear
            </button>
            <button class="btn btn-info" id="btnDocumentos" disabled>
                <i class="icon-stack4"></i>  Documentos
            </button>
            @if($afiliado->idrol == 1 or $afiliado->idrol == 3 or $afiliado->idrol == 4)

            <a href="nueva_ocurrencia" class="btn btn-info">
                <i class="icon-file-plus"></i>
                Nueva Ocurrencia
            </a>
            <button class="btn bg-indigo" id="btnModificar">
                <i class="icon-pencil5"></i>
                Modificar
            </button>
            <button class="btn bg-success" id="btnEnviar">
                <i class="icon-paperplane"></i>
                 Enviar
            </button>
            <button class="btn btn-danger" id="btnEliminar">
                <i class="icon-trash-alt"></i> Eliminar
            </button>
            <button class="btn btn-danger" id="btnCerrar">
                <i class="icon-folder-check"></i> Cerrar
            </button>
            <button class="btn btn-success" id="btnDocumento">
                    <i class="icon-file-text2"></i> Documento
            </button>
            @endif
            @if($afiliado->idrol == 2 or $afiliado->idrol == 3 or $afiliado->idrol == 4)
                <button class="btn btn-success" id="btnAprobar" disabled>
                    <i class="icon-checkmark4"></i> Aprobar
                </button>
                <button class="btn bg-danger" id="btnRechazar" disabled>
                    <i class="icon-cross2"></i> Rechazar
                </button>
                <button class="btn btn-info" id="btnModificacion" disabled>
                    <i class="icon-warning"></i> Rechazar para Modificacion
                </button>
            @endif


        </div>
    </div>
    <div class="row div-row" id="formulario">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Colaborador</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="colaborador" placeholder="Colaborador" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Falta</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="falta" placeholder="falta" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Sanción</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="sancion" placeholder="Sancion" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Reincidencia</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="reincidencia" placeholder="incidencia"disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Descripción</label>
                </div>
                <div class="col-md-10">
                    <textarea type="text" id="descripcion"  style="width:100%;" disabled></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Estado</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="estado" placeholder="estado" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> F. Ocurrencia</label>
                </div>
                <div class="col-md-10">
                    <input type="text" id="fec_ocurrencia" placeholder="YY-Mm-Dd" disabled>
                </div>
            </div>
            <div class="form-group" id="div-suspencion">
                <div class="col-md-2">
                    <label for=""> F. Suspencion</label>
                </div>
                <div class="col-md-5">
                    <input type="text" id="fec_suspencion_inicio" placeholder="YY-Mm-Dd" disabled>
                </div>
                <div class="col-md-5">
                    <input type="text" id="fec_suspencion_fin" placeholder="YY-Mm-Dd" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Comentario</label>
                </div>
                <div class="col-md-10">
                    <textarea type="text" id="comentario"  style="width:100%;" disabled></textarea>
                </div>
            </div>
            @if($afiliado->idrol == 2 or $afiliado->idrol == 3 or $afiliado->idrol == 4)

            <div class="form-group">
                <div class="col-md-2">
                    <label for=""> Comentario Gerencia</label>
                </div>
                <div class="col-md-10">
                    <textarea type="text" id="gerencia_descripcion"  style="width:100%;" disabled></textarea>
                </div>
            </div>
                @endif
        </div>
    </div>
    <div class="admin-content">
        <div class="content-oh">
            <div class="log-table">

                <div class="table-responsive">
                    <table class="table ">
                        <thead class="thead-dark-lite">
                        <tr>
                            <th ></th>
                            <th width="50">ID</th>
                            <th>Fecha</th>
                            <th width="100">Estado</th>
                            <th>Afiliado</th>
                            <th width="300" >Falta</th>
                            <th>Sanción</th>
                            <th># Docs</th>
                            <th >Descripción</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($data['ocurrencias']->ocurrencias->data as $index => $ocurrencia)
                            <tr>
                                <td scope="row table-actions-button">
                                    <?php
                                    switch ($ocurrencia->estado){

                                        case 3:
                                            echo '<img width="20" src="'.asset("assets\img\aprobado.png").'">';
                                            break;
                                        case 4:
                                            echo '<img width="20" src="'.asset("assets\img\denegado.png").'">';
                                            break;
                                        case 5:
                                            echo '<img width="20" src="'.asset("assets\img\aprobado.png").'">';
                                            break;
                                        case 6:
                                            echo '<img width="20" src="'.asset("assets\img\denegado.png").'">';
                                            break;
                                        default:
                                            echo '<img width="20" src="'.asset("assets\img\pendiente.png").'">';
                                            break;

                                    }
                                    ?>
                                </td>
                                <td  id="tr-ocurrencia" data-id="{{$index}}">{{ $ocurrencia->idocurrencia }}</td>
                                @if($ocurrencia->prioridad == 2)
                                    <td  id="tr-ocurrencia" data-id="{{$index}}">
                                        <span class="red">{{ $ocurrencia->fec_ocurrencia }}</span>
                                    </td>
                                @elseif($ocurrencia->prioridad == 1)
                                    <td  id="tr-ocurrencia" data-id="{{$index}}">
                                        <span class="green">{{ $ocurrencia->fec_ocurrencia }}</span>
                                    </td>
                                @else
                                    <td  id="tr-ocurrencia" data-id="{{$index}}">{{ $ocurrencia->fec_ocurrencia }}</td>

                                @endif
                                <td id="tr-ocurrencia" data-id="{{$index}}">
                                   <?php
                                        switch ($ocurrencia->estado){
                                            case -1:
                                                echo "Borrador";
                                                break;
                                            case 0:
                                                echo "Enviado Aprobador";
                                                break;
                                            case 1:
                                                echo "Rechazado para modifiación";
                                                break;
                                            case 2:
                                                echo "Modificado";
                                                break;
                                            case 3:
                                                echo "Aceptado Gerencia";
                                                break;
                                            case 4:
                                                echo "Denegado Gerencia";
                                                break;
                                            case 5:
                                                echo "Aprobado Relab";
                                                break;
                                            case 6:
                                                echo "Denegado Relab";
                                                break;
                                            case 7:
                                                echo "Cerrado";
                                                break;
                                            case 8:
                                                echo "Enviado a Relab";
                                                break;

                                        }
                                        ?>

                                </td>
                                <td  id="tr-ocurrencia" data-id="{{$index}}">{{ $ocurrencia->nombres }}</td>
                                <td  id="tr-ocurrencia" data-id="{{$index}}">{{ $ocurrencia->falta }}</td>
                                <td  id="tr-ocurrencia" data-id="{{$index}}">{{ $ocurrencia->sancion }}</td>
                                <td  id="tr-ocurrencia" data-id="{{$index}}">{{ count($ocurrencia->documentos) }}</td>
                                <td  id="tr-ocurrencia" data-id="{{$index}}">{{ $ocurrencia->descripcion}}</td>
                            </tr>
                            @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="pull-left">
                </div>

            </div>

    </div>
        <div class="modal fade bs-example-modal-sm" id="rastreo-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="titulo">                    Rastreo de la Ocurrencia
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div id="auxliar"></div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tbody id="table-rastreo">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <script type="application/javascript">

        var ocurrencias = "{{ json_encode($data['ocurrencias']->ocurrencias->data ) }}";
        var rol = "{{ json_encode(Auth::user()->idrol ) }}";
        ocurrencias = JSON.parse(ocurrencias.replace(/&quot;/g,'"'));
        var ocurrencia;
        $('#btnModificar').hide();
        $('#btnEnviar').hide();
        $('#btnEliminar').hide();
        $('#btnCerrar').hide();
        $('#btnDocumento').hide();
        $('#div-suspencion').hide();

        var tr;
        $('tbody').on('click','tr #tr-ocurrencia',function(){
            var id = $(this).data('id');
            if(tr){
                tr.removeClass('bg-blue-400');
            }
            tr = $(this).parent();
            tr.addClass("bg-blue-400");
            ocurrencia = ocurrencias[id];
            $('#colaborador').val(ocurrencia.nombres);
            $('#falta').val(ocurrencia.falta);
            $('#sancion').val(ocurrencia.sancion);
            if(ocurrencia.idsancion == 1){
                $('#div-suspencion').show();
                $('#fec_suspencion_inicio').val(ocurrencia.fec_suspencion_ini);
                $('#fec_suspencion_fin').val(ocurrencia.fec_suspencion_fin);
            }else{
                $('#div-suspencion').hide();
                $('#fec_suspencion_inicio').val('');
                $('#fec_suspencion_fin').val('');
            }
            $('#reincidencia').val(1);
            var estado = "";
            switch(ocurrencia.estado) {
                case -1:
                    estado = "Borrador";
                    break;
                case 0:
                    estado = "Enviado Aprobador";
                    break;
                case 1:
                    estado = "Rechazado Modificacion";
                    break;
                case 2:
                    estado = "Modificado | Pendiente";
                    break;
                case 3:
                    estado = "Aprobado Gerencia";
                    break;
                case 4:
                    estado = "Rechazado Gerencia";
                    break;
                case 5:
                    estado = "Aprobado Relab";
                    break;
                case 6:
                    estado = "Rechazado Relab";
                    break;
                case 7:
                    estado = "Cerrado";
                    break;
            }
            $('#estado').val(estado);
            $('#fec_ocurrencia').val(ocurrencia.fec_ocurrencia);
            var nombres_doc = "";
            if(ocurrencia.documentos !== null){
                $.each(ocurrencia.documentos,function(index,value){
                    nombres_doc += value.nombre+"\n";
                });
                $('#documentos').html(nombres_doc);
            }

            $('#descripcion').html(ocurrencia.descripcion);
            $('#comentario').html(ocurrencia.comentarios);
             if(ocurrencia.estado == -1){
                 $('#btnEliminar').show();
                 $('#btnEnviar').show();

             }else{
                 $('#btnEliminar').hide();
                 $('#btnEnviar').hide();
             }
             $('#btnDocumentos').prop("disabled", false);
            if(ocurrencia.estado == 1 || rol == 4 || rol == 2 ){
                $('#btnModificar').show();
            }else{
                $('#btnModificar').hide();
            }
            if((ocurrencia.estado == 0 || ocurrencia.estado == 2 || ocurrencia.estado == 3) && (rol == 3 || rol == 2 || rol == 4)){
                $('#gerencia_descripcion').attr('disabled',false);
                $('#btnAprobar').attr('disabled',false);
                $('#btnRechazar').attr('disabled',false);
                $('#btnModificacion').attr('disabled',false);

            }else{
                $('#gerencia_descripcion').attr('disabled',true);
                $('#btnAprobar').attr('disabled',true);
                $('#btnRechazar').attr('disabled',true);
                $('#btnModificacion').attr('disabled',true);
            }
            if(ocurrencia.estado > 2 && ocurrencia.estado < 7){
                if(ocurrencia.estado == 5 || (ocurrencia.estado == 3 && ocurrencia.idsancion != 1 )){
                        $('#btnCerrar').show();
                        $('#btnCerrar').attr('disabled',false);
                }else{
                        $('#btnCerrar').hide();
                        $('#btnCerrar').attr('disabled',true);
                }
                if((ocurrencia.estado == 3 && ocurrencia.idsancion != 1) || (ocurrencia.estado == 5 && ocurrencia.idsancion == 1)){
                    $('#btnDocumento').show();
                    $('#btnDocumento').attr('disabled',false);
                }
                else{
                    $('#btnDocumento').hide();
                }
            }else {

                    $('#btnDocumento').hide();
                    $('#btnDocumento').attr('disabled',true);
                    $('#btnCerrar').hide();
                    $('#btnCerrar').attr('disabled',true);
            }

            if( ocurrencia.estado == 3 && ocurrencia.idsancion > 1 && rol == 4  ){
                $('#btnAprobar').attr('disabled',true);
                $('#btnRechazar').attr('disabled',true);
                $('#btnModificacion').attr('disabled',true);
            }else if(ocurrencia.estado == 3 && ocurrencia.idsancion == 1 && rol == 4 ){
                $('#btnAprobar').attr('disabled',false);
                $('#btnAprobar').html('Aprobar Relab');
                $('#btnRechazar').attr('disabled',false);
                $('#btnRechazar').html('Denegar Relab');
                $('#btnModificacion').attr('disabled',false);
                $('#btnModificacion').html('Pedir Modificacion Relab');
            }else{
                $('#btnAprobar').html('Aprobar');
                $('#btnRechazar').html('Denegar');
                $('#btnModificacion').html('Pedir Modificacion');
            }
            if ((ocurrencia.estado == 4 || ocurrencia.estado == 6) && rol == 4 ){
                $('#btnCerrar').show();
                $('#btnCerrar').attr('disabled',false);
            }

            $('html, body').animate({
                scrollTop: $("#navbar-mobile").offset().top
            }, 500);

        });

       $('#rastrear').click(function(){
           if(ocurrencia){
               $.post('qrastreo',{'idocurrencia':ocurrencia.idocurrencia},function(data){
                   var rastreo = JSON.parse(data);
                   var filas = "";
                   filas += " <tr> <th> Estado</th> <th> Comentario </th>"
                   $.each(rastreo.data,function(index,value){
                       filas += '<tr>' +
                           '                                    <td  id="tr-ocurrencia">'+value.tipo+'<br>'+value.fecha_creacion+'</td>' +
                           '                                    <td  id="tr-ocurrencia">'+value.detalle +'</td>' +
                           '                                </tr>';
                   });
                   $('#table-rastreo').html('').append(filas);
                   $('#titulo').html('').append('<h4>Rastreo de la ocurrencia '+ocurrencia.idocurrencia+'</h4>');
                   $('#rastreo-modal').modal();

               });
           }else{
               var mensaje = '<div class="alert alert-info alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0">' +
                   '                <span class="font-weight-semibold">Upps!</span> Escoge una Ocurrencia.' +
                   '            </div>';
               $('#table-rastreo').html('').append(mensaje);
               $('#rastreo-modal').modal();
           }


       });
       $('#btnDocumentos').click(function(){
           var formulario = '<div class="row">' +
               '        <div class="col-md-12" >' +
               '            <div class="form-group row">' +
               '                <label for="archivos" class="col-sm-2 col-form-label">Cargar Documentos:</label>' +
               '                <div class="col-sm-5">' +
               '                    <div class="file-loading border-teal">' +
               '                        <input id="archivos" class="file" type="file" multiple>' +
               '                        <span class="form-text text-muted text-teal text-right">Debes grabar primero la ocurrencia </span> <br>' +
               '                        <span class="form-text text-muted text-danger text-right">Los archivos no pueden pesar mas de 5MB </span>' +
               '                        <input type="hidden" id="ocurrencia" class="border-teal" value="">' +
               '                    </div>' +
               '                    <div class="mensaje"></div>' +
               '                </div>' +
               '                <div class="col-sm-5">' +
               '                    <div class="file-loading border-teal">' +
               ' <button class="btn btn-info" id="subir" >' +
               '                <i class="icon-floppy-disk"></i>' +
               '                Subir Arhivos' +
               '            </button>' +
               '                    </div>' +
               '                </div>' +
               '            </div>' +
               '        </div>' +
               '    </div>' +
               '    <div class="row div-row">' +
               '        <div class="row" id="doc-card">' +
               '        </div>' +
               '    </div>';
            if(ocurrencia.documentos){
                var filas ="";
                        $.each(ocurrencia.documentos,function(index,value){
                            filas += '<tr>' +
                                '                                    <td  id="tr-ocurrencia">'+value.nombre+'</td>' +
                                '                                    <td  id="tr-ocurrencia">'+value.extencion +'</td>' +
                                '                                    <td  id="tr-ocurrencia"><a class="btn btn-info" target="_blank" href="'+value.url +'" ><i class="icon-arrow-down8"></i>Descargar </a></td>' +
                                '                                </tr>';
                        });

                    $('#auxliar').html('').append(formulario);
                    $('#table-rastreo').html('').append(filas);
                    $('#titulo').html('').append('<h4>Documentos de la ocurrencia '+ocurrencia.idocurrencia+'</h4><br><p style="font-weight: lighter; color: #000">'+ocurrencia.descrip_documento+'</p>');
                    $('#rastreo-modal').modal();

            }else{
                var mensaje = '<div class="alert alert-info alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0">' +
                    '                No hay Documentos asignados a esta ocurrencia.' +
                    '            </div>';

                $('#table-rastreo').html('').append(mensaje);
                $('#auxliar').html('').append(formulario);
                $('#titulo').html('').append('<h4>Documentos de la ocurrencia '+ocurrencia.idocurrencia+'</h4><br><p style="font-weight: lighter; color: #000">'+ocurrencia.descrip_documento+'</p>');
                $('#rastreo-modal').modal();
            }


        });
       /*************   AGREGAR DOCUMENTOS***********/

        var files = new FormData();
        var files_array = [];


        $("#auxliar").on('change','.row .col-sm-5 #archivos',function(){
            console.log('cambio');
            var archivos = $(this).prop("files");
            var card = "";

            for (var i = 0; i < archivos.length; i++) {
                var FileSize = archivos[i].size / 1024 / 1024; // in MB
                if (FileSize > 5) {
                    swal({
                        title: 'Upps!',
                        text: "El archivo "+ archivos[i].name +" es muy grande" ,
                        icon: 'warning'
                    })
                } else {
                    files_array.push(archivos[i]);
                    var index = files_array.indexOf(archivos[i]);
                    card += '            <div class="col-md-4" > ' +
                        '                <div class="card">' +
                        '                    <div class="card-body">' +
                        '                        <div class="row">' +
                        '                            <div class="col-md-8">' +
                        '                        <h7 class="card-title">'+ archivos[i].name +' </h7>' +
                        '                            </div>' +

                        '                            <div class="col-md-3">' +

                        '                   <button type="button" id="quitar" data-id="'+ index +'" class="btn btn-warning rounded-round btn-sm"><i class="icon-cross3"></i> </button>' +

                        '                            </div>' +
                        '                        </div>' +
                        '                    </div>' +
                        '                </div>' +
                        '            </div>'

                }
            }
            $('#doc-card').append(card);
        });

        $('#auxliar').on('click','#doc-card .card #quitar',function(){
            var id = $(this).data('id');
            $(this).parent().parent().parent().parent().parent().hide();
            /**COMPROBAR SI HAY EMPTYS***/
            delete files_array[id];
            console.log(files_array);
        });

        $('#auxliar').on('click','.row .col-sm-5 #subir',function(){
            var idocurrencia = ocurrencia.idocurrencia;
            if(files_array.length > 0 ){
                for (var i = 0; i < files_array.length; i++) {
                    if(files_array[i]){
                        var files = new FormData();
                        files.append('idocurrencia',idocurrencia);
                        files.append('archivo',files_array[i]);
                        jQuery.ajax({
                            url: '{{ route('quploadfile_ocurrencia') }}',
                            data: files,
                            cache: false,
                            contentType: false,
                            processData: false,
                            method: 'POST',
                            type: 'POST', // For jQuery < 1.9
                            success: function(response){
                                swal({
                                    title: 'Listo!',
                                    text: 'Se subieron los documentos',
                                    icon: 'success'
                                }).then(function() {
                                    $('#btnEnviar').attr('disabled',false);
                                });
                            }
                        });

                    }
                }
            }else{
                swal({
                    title: 'Upps!',
                    text: 'Agrega un nuevo Documento!',
                    icon: 'warning'
                })
            }
            files_array = [];
        });

       $('#btnModificar').click(function(){
           window.location.href = "{{ route('editar_ocurrencias') }}?ocurrencia="+ocurrencia.idocurrencia;
       });

       $('#btnEliminar').click(function(){
           if(ocurrencia.estado === -1){
               swal({
                   title: "Eliminar",
                   text: "Seguro que deseas eliminar la ocurrencia?",
                   icon: "error",
                   buttons: [
                       'No, cancelar!',
                       'Si'
                   ],
                   dangerMode: true,
               }).then(function(isConfirm) {
                   if (isConfirm) {
                       $.post('qeliminar_ocurrencia',{'idocurrencia':ocurrencia.idocurrencia},function(data){
                           swal({
                               title: 'Listo!',
                               text: 'Se elimino la ocurrencia',
                               icon: 'success',
                               dangerMode: true,
                           }).then(function() {
                               window.location.href = "{{ route('listas') }}";
                           });
                       });
                   }
               });
           }
       });
       $('#btnEnviar').click(function(){
            if(ocurrencia.estado === -1){
                swal({
                    title: "Enviar",
                    text: "Seguro que deseas enviar la ocurrencia?",
                    icon: "info",
                    buttons: [
                        'No, cancelar!',
                        'Si'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.post('{{ route('qenviar_ocurrencias') }}',{'idocurrencia':ocurrencia.idocurrencia},function(data){
                            swal({
                                title: 'Listo!',
                                text: 'Se envio la ocurrencia',
                                icon: 'success',
                                dangerMode: true,
                            }).then(function() {
                                window.location.href = "{{ route('listas') }}";
                            });
                        });
                    }
                });
            }
        });
        $('#btnAprobar').click(function(){
            var descripcion =  $('#gerencia_descripcion').val();
            if(ocurrencia.estado === 0 || ocurrencia.estado == 2 || (ocurrencia.estado == 3 && ocurrencia.idsancion == 1 && (rol == 3 || rol == 4))){
                swal({
                    title: "Aprobar Ocurrencia",
                    text: "Seguro que deseas aprobar la ocurrencia?",
                    icon: "success",
                    buttons: [
                        'No, cancelar!',
                        'Enviar'
                    ],
                    dangerMode: true
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        // revisar estos estados de acpetacion
                        if( (rol == 3 && ocurrencia.estado === 3) || (rol == 4 && ocurrencia.estado === 8) || (rol == 4 && ocurrencia.estado === 3) ){
                            console.log('entro');
                            $.post('{{ route('qaceptadorelab') }}',{idocurrencia:ocurrencia.idocurrencia,idcolaborador:ocurrencia.idafiliado, descripcion:descripcion,tipo:4},function(data){
                                swal({
                                    title: 'Listo!',
                                    text: 'Se envio la ocurrencia',
                                    icon: 'success',
                                    dangerMode: true,
                                }).then(function() {
                                    window.location.href = "{{ route('listas') }}";
                                });
                            });
                        }
                        else{
                            $.post('{{ route('qaceptado') }}',{idocurrencia:ocurrencia.idocurrencia,idcolaborador:ocurrencia.idafiliado, descripcion:descripcion},function(data){
                                swal({
                                    title: 'Listo!',
                                    text: 'Se envio la ocurrencia',
                                    icon: 'success',
                                    dangerMode: true,
                                }).then(function() {
                                    window.location.href = "{{ route('listas') }}";
                                });
                            });
                        }

                    }
                });
            }
        });
        $('#btnRechazar').click(function(){
            var descripcion =  $('#gerencia_descripcion').val();
            if(ocurrencia.estado === 0 || ocurrencia.estado == 2 || (ocurrencia.estado == 3 && ocurrencia.idsancion == 1 && (rol == 3 || rol == 4))){
                swal({
                    title: "Rechazar Ocurrencia",
                    text: "Seguro que deseas rechazar la ocurrencia?",
                    icon: "error",
                    buttons: [
                        'No, cancelar!',
                        'Enviar'
                    ],
                    dangerMode: true
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        if( (rol == 3 && ocurrencia.estado === 3) || (rol == 4 && ocurrencia.estado === 8) || (rol == 4 && ocurrencia.estado === 3) ){
                            $.post('{{ route('qdenegadorelab') }}',{idocurrencia:ocurrencia.idocurrencia,idcolaborador:ocurrencia.idafiliado, descripcion:descripcion},function(data){
                               var obj = JSON.parse(data);
                                swal({
                                    title: 'Listo!',
                                    text: obj.mensaje,
                                    icon: 'success',
                                    dangerMode: true,
                                }).then(function() {
                                    window.location.href = "{{ route('listas') }}";
                                });
                            });
                        }else{
                            $.post('{{ route('qdenegado') }}',{idocurrencia:ocurrencia.idocurrencia,idcolaborador:ocurrencia.idafiliado, descripcion:descripcion},function(data){
                                var obj = JSON.parse(data);

                                swal({
                                    title: 'Listo!',
                                    text: obj.mensaje,
                                    icon: 'success',
                                    dangerMode: true,
                                }).then(function() {
                                    window.location.href = "{{ route('listas') }}";
                                });
                            });
                        }
                    }
                });
            }
        });
        $('#btnModificacion').click(function(){
                swal({
                    title: "Rechazar para Modificacion",
                    text: "Escribe un comentario para la modificacion",
                    icon: "warning",
                    content: "input",
                    buttons: [
                        'No, cancelar!',
                        'Enviar'
                    ],
                    dangerMode: true,
                    inputPlaceholder: "Comentario"
                }).then(function(value) {
                    if (value) {
                        if(rol == 3){
                            $.post('{{ route('qpedir_modificacion') }}',{idocurrencia:ocurrencia.idocurrencia,idcolaborador:ocurrencia.idafiliado,tipo:4,descripcion:value},function(data){
                                var obj = JSON.parse(data);
                                swal({
                                    title: 'Listo!',
                                    text: obj.mensaje,
                                    icon: 'success',
                                    dangerMode: true,
                                }).then(function() {
                                    window.location.href = "{{ route('listas') }}";
                                });
                            });
                        }else{
                            $.post('{{ route('qpedir_modificacion') }}',{idocurrencia:ocurrencia.idocurrencia,idcolaborador:ocurrencia.idafiliado,descripcion:value},function(data){
                                var obj = JSON.parse(data);

                                swal({
                                    title: 'Listo!',
                                    text: obj.mensaje,
                                    icon: 'success',
                                    dangerMode: true
                                }).then(function() {
                                    window.location.href = "{{ route('listas') }}";
                                });
                            });
                        }

                    }
                });
        });
        $('#btnCerrar').click(function(){
            if(ocurrencia.estado >2 && ocurrencia.estado < 7){
                swal({
                    title: "Cerrar Ocurrencia",
                    text: "Debes subir el formato firmado antes de cerrar la ocurrencia.\n Seguro que quieres cerrar la ocurrencia?",
                    icon: "error",
                    buttons: [
                        'No, cancelar!',
                        'Si'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.post('{{ route('qcerrar') }}',{'idocurrencia':ocurrencia.idocurrencia},function(data){
                            swal({
                                title: 'Listo!',
                                text: 'Se envio la ocurrencia',
                                icon: 'success',
                                dangerMode: true,
                            }).then(function() {
                                window.location.href = "{{ route('listas') }}";
                            });
                        });
                    }
                });
            }
        });

        $('#btnDocumento').click(function(){
            window.open("{{ route("qarchivo_pdf")  }}?idocurrencia="+ocurrencia.idocurrencia);
        });

   </script>
@endsection
