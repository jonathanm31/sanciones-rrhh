@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="assets/css/styles.css">
    <meta name="csrf-token" content="{{ Session::token() }}">



    <div class="admin-content">

        <div class="content-oh">

            <form class="sanciones">
                <div class="conteiner-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="main-actions">

                                {{--<button type="button" class="btn btn-primary"><img src="assets/img/buscar.svg"> Buscar</button>--}}
                                <button type="button" id="guardar" class="btn btn-primary"><img src="assets/img/nuevo.svg">Guardar</button>
                                {{--<button type="button" class="btn btn-primary"><img src="assets/img/exportar.svg"> Exportar</button>--}}

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">


                            <div class="form-group row">
                                <label for="reincidencias-input" class="col-sm-2 col-form-label">Nombre</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nombre" placeholder="Todos">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="empresa-select" class="col-sm-2 col-form-label">Estado</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" id="estado">
                                        <option value="0" selected>Activo</option>
                                        <option value="1">Inactivo</option>
                                    </select>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </form>

            <div class="log-table">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="thead-dark-lite">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Sancion</th>
                            <th scope="col">Estado</th>
                        </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>
            </div>


        </div>

    </div>

    <script src="assets/js/scripts.js?v=0.1"></script>

    <script type="application/javascript">

        $.post('sanciones_all',{ '_token': $('meta[name=csrf-token]').attr('content')},function(data){
            var faltas = JSON.parse(data);
            var filas = "";
            $.each(faltas.data,function(index,value){
                filas += '<tr>' +
                    '                            <th scope="row table-actions-button">' +
                    '' +
                    '                                    <img src="assets/img/aprobado.svg">' +
                    '                                <button type="button" class="btn btn-link" id="eliminar" data-id="'+value.idsancion+'">' +
                    '                                  <i class="glyphicon glyphicon-trash text-danger"></i>' +
                    '                                </button>' +
                    '' +
                    '                            </th>' +
                    '                            <td>'+value.sancion+'</td>' +
                    '                            <td>'+value.state+'</td>' +
                    '                        </tr>';   
            });
            $('tbody').append(filas);
        });

        $('tbody').on('click','#eliminar',function(){
            var id = $(this).data('id');
            swal({
                title: "¿Está seguro que deseas eliminar.",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "CONFIRMAR",
                closeOnConfirm: false,
                closeOnCancel: false
            },function(isConfirm) {
                if (isConfirm) {
                    var razon = $('#razon').val();
                    $.post('eliminar_saciones',{ '_token': $('meta[name=csrf-token]').attr('content') , idsancion:id},function(data){
                        var response = JSON.parse(data);
                        swal({
                            text: response.mensaje,
                            icon: "success",
                        });
                        location.reload();
                    });
                } else {
                    swal("Ok, saliste!");

                }
            });
        });

        $('#guardar').click(function(){
            var nombre = $('#nombre').val();
            var estado = $('#estado').val();
            $.post('insertar_sanciones',{ '_token': $('meta[name=csrf-token]').attr('content') , nombre:nombre,estado:estado},function(data){
                var response = JSON.parse(data);
                swal({
                    title: "Se envio el formulario.",
                    text: response.mensaje,
                    icon: "success",
                });
                location.reload();
            });
        });


    </script>
@endsection
